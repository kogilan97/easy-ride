package com.finalyearproject.easyride.data;

public class Result {
    public String request_id ;
    public String status;
    public String event_id;
    public String price;
    public String currency;
    public String estimated_price_messages_sent;

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getEstimated_price_messages_sent() {
        return estimated_price_messages_sent;
    }

    public void setEstimated_price_messages_sent(String estimated_price_messages_sent) {
        this.estimated_price_messages_sent = estimated_price_messages_sent;
    }
}
