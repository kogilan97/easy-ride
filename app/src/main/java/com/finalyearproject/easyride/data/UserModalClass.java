package com.finalyearproject.easyride.data;

import com.finalyearproject.easyride.easyride.BookJourney.model.Path;
import com.finalyearproject.easyride.easyride.Driver.model.PathInfo;
import com.finalyearproject.easyride.easyride.Earnings.model.EarningInfo;
import com.finalyearproject.easyride.easyride.Journey.model.JourneyInfo;
import com.finalyearproject.easyride.easyride.LoginRegister.model.UserInfo;
import com.finalyearproject.easyride.easyride.Payment.model.Card;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserModalClass {


    @SerializedName("token")
    private String token;

    @SerializedName("status")
    private Boolean status;

    @SerializedName("message")
    private String message;

    @SerializedName("userInfo")
    private UserInfo userInfo;

    @SerializedName("journeyInfo")
    private List<JourneyInfo> journeyInfoList;

    @SerializedName("journey")
    private JourneyInfo journeyInfo;

    @SerializedName("paths")
    private List<Path> paths;

    @SerializedName("path")
    private Path path;

    @SerializedName("card")
    private Card card;

    @SerializedName("result")
    private Result result;

    @SerializedName("pathInfo")
    public List<PathInfo> pathInfo;

    @SerializedName("totalCost")
    public String totalCost;

    @SerializedName("earningsArray")
    private List<EarningInfo> earningInfo;

    @SerializedName("totalAmount")
    private String totalAmount;


    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public List<EarningInfo> getEarningInfo() {
        return earningInfo;
    }

    public void setEarningInfo(List<EarningInfo> earningInfo) {
        this.earningInfo = earningInfo;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    public List<PathInfo> getPathInfo() {
        return pathInfo;
    }

    public void setPathInfo(List<PathInfo> pathInfo) {
        this.pathInfo = pathInfo;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public JourneyInfo getJourneyInfo() {
        return journeyInfo;
    }

    public void setJourneyInfo(JourneyInfo journeyInfo) {
        this.journeyInfo = journeyInfo;
    }

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    public List<Path> getPaths() {
        return paths;
    }

    public void setPaths(List<Path> paths) {
        this.paths = paths;
    }

    public List<JourneyInfo> getJourneyInfoList() {
        return journeyInfoList;
    }

    public void setJourneyInfoList(List<JourneyInfo> journeyInfoList) {
        this.journeyInfoList = journeyInfoList;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }



}
