package com.finalyearproject.easyride;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import androidx.fragment.app.Fragment;

import com.finalyearproject.easyride.database.EasyRideStorage;
import com.finalyearproject.easyride.di.component.ApplicationComponent;
import com.finalyearproject.easyride.di.component.DaggerApplicationComponent;
import com.finalyearproject.easyride.di.module.ContextModule;
import com.microblink.MicroblinkSDK;

public class EasyRideApplication extends Application {

    private static Context mContext;
    ApplicationComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        EasyRideStorage.getInstance().init(this);
        mContext=this;
        initDaggerInjection();

        MicroblinkSDK.setLicenseKey("sRwAAAAdY29tLmZpbmFseWVhcnByb2plY3QuZWFzeXJpZGVQhs7jCvPdd9ASuCT/hr1ooZ0k6pVj2MOL0QuJOUsDtXdZz7C3ubQhU/E0Loqz4zzyDrME1aEXKcO7yrfaKhuHmqPIzCozl4P/ZHIjDbnPEPy3BxJiZ2hxD0ktzW2NTEecr9P6qlJsZ9Iv4OaZwgLVE/xBKGqOg+1yqD+f+tvrk1GVKAIoIA92pVlSsVcv37mE8UNwZfMuHrPz1MVavqnjynTegps4WOuaZKdfFYVmAP0zoyGFm/k+IBMmEhZUtejEUoXBicGjuCCrCfS8bGQ=", this);
    }

    private void initDaggerInjection() {
        mAppComponent = DaggerApplicationComponent.builder().contextModule(new ContextModule(this)).build();
        mAppComponent.injectApplication(this);
    }

    public static Context getContext() {
        return mContext;
    }
    public static EasyRideApplication get(Activity activity) {
        return (EasyRideApplication) activity.getApplication();
    }

    public static EasyRideApplication get(Fragment fragment) {
        Activity activity = fragment.getActivity();
        if (activity != null) {
            return (EasyRideApplication) fragment.getActivity().getApplication();
        }
        return null;
    }

    public ApplicationComponent getAppComponent() {
        return mAppComponent;
    }
}
