package com.finalyearproject.easyride;


import static com.finalyearproject.easyride.easyride.utils.Utils.dpToPx;

public class EasyRideConstants {
    public static String localhost = "http://localhost:8081/";
    public static String EBHost = "http://easyridebackend-dev.us-east-1.elasticbeanstalk.com/";
    public static String displayerror = "An error has occured";

    public static final float DEFAULT_TEXT_SIZE = dpToPx(14);
    public static final float DEFAULT_SWIPE_DISTANCE = 0.85f;
    public static final int BTN_INIT_RADIUS = dpToPx(15);
    public static final int BTN_MORPHED_RADIUS = dpToPx(40);
    public static final int MORPH_ANIM_DURATION = 500;

    // User role
    public static final int ROLE_USER = 1;
    public static final int ROLE_DRIVER = 2;


}
