package com.finalyearproject.easyride.easyride.Feedback;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.finalyearproject.easyride.R;
import com.finalyearproject.easyride.data.UserModalClass;
import com.finalyearproject.easyride.database.EasyRideStorage;
import com.finalyearproject.easyride.di.module.APIInterface;
import com.finalyearproject.easyride.di.module.RetrofitModule;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class RiderContactUsFragment extends Fragment {
private Button btncallsupport;
private Button btnsubmitfeedback;
private EditText txtFeedback;


private APIInterface apiInterface;
    public static RiderContactUsFragment getInstance() {
        return new RiderContactUsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rider_contactus, container, false);
        btncallsupport = view.findViewById(R.id.btncallsupport);
        btnsubmitfeedback = view.findViewById(R.id.btnfeedbacksubmit);
        txtFeedback=view.findViewById(R.id.txtFeedback);
        btncallsupport.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
             callPhoneNumber();
            }
        });

        btnsubmitfeedback.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view)
            {
                if(!TextUtils.isEmpty(txtFeedback.getText()))
                {
                    sendFeedback();
                }
                else
                {
                    Toast.makeText(getContext(), "The feedback field cannot be empty", Toast.LENGTH_SHORT).show();

                }
            }
        });
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Rider Contact Us");
    }

    public void callPhoneNumber()
    {
        try
        {
            if(Build.VERSION.SDK_INT > 22)
            {
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                   requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, 101);
                    return;
                }

                Uri u = Uri.parse("tel:" + "+60163574937");
                Intent i = new Intent(Intent.ACTION_DIAL, u);
                startActivity(i);

            }
            else {
                Uri u = Uri.parse("tel:" + "+60163574937");
                Intent i = new Intent(Intent.ACTION_DIAL, u);
                startActivity(i);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void sendFeedback()
    {

        JSONObject object = new JSONObject();
        try {

            object.put("userId", EasyRideStorage.getInstance().getPrefUserId());
            object.put("email", EasyRideStorage.getInstance().getPrefEmailAddr());
            object.put("comment",txtFeedback.getText().toString());

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("raw"),object.toString());
        apiInterface = RetrofitModule.getApiInterface();
        apiInterface.getFeedback(EasyRideStorage.getInstance().getAuthToken(),body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {
                    @Override
                    public void onCompleted() {

                    }
                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onNext(UserModalClass userModalClass) {
                        Toast.makeText(getContext(), userModalClass.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

    }
}




