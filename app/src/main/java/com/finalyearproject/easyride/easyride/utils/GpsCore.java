package com.finalyearproject.easyride.easyride.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.LocaleList;
import android.provider.Settings;

import com.finalyearproject.easyride.EasyRideApplication;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class GpsCore {public static Address getAddress(double lat, double lng, Context context) throws IOException {
    Geocoder geocoder = new Geocoder(context, Locale.getDefault());
    List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
    Address obj = addresses.get(0);
    return obj;
}

    public static void turnGPSOn() {
        String provider = Settings.Secure.getString(EasyRideApplication.getContext().getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if (!provider.contains("gps")) { //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            EasyRideApplication.getContext().sendBroadcast(poke);
        }
    }

    public static void turnGPSOff() {
        String provider = Settings.Secure.getString(EasyRideApplication.getContext().getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if (provider.contains("gps")) { //if gps is enabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            EasyRideApplication.getContext().sendBroadcast(poke);
        }
    }

    public static boolean canToggleGPS() {
        PackageManager pacman = EasyRideApplication.getContext().getPackageManager();
        PackageInfo pacInfo = null;

        try {
            pacInfo = pacman.getPackageInfo("com.android.settings", PackageManager.GET_RECEIVERS);
        } catch (PackageManager.NameNotFoundException e) {
            return false; //package not found
        }

        if (pacInfo != null) {
            for (ActivityInfo actInfo : pacInfo.receivers) {
                //test if recevier is exported. if so, we can toggle GPS.
                if (actInfo.name.equals("com.android.settings.widget.SettingsAppWidgetProvider") && actInfo.exported) {
                    return true;
                }
            }
        }

        return false; //default
    }

    public static String getPhoneLocale(Context context) {
        String locale = "";
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            LocaleList list = context.getResources().getConfiguration().getLocales();
            if (list.size() > 0) {
                locale = list.get(0).getDisplayCountry();
            }
        } else {
            locale = context.getResources().getConfiguration().locale.getDisplayCountry();
        }

        return locale;
    }

    public static String getCityCountryNameByCoordinates(Context context, double lat, double lon) throws IOException {

        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addresses = geocoder.getFromLocation(lat, lon, 1);
        if (addresses != null && addresses.size() > 0) {
            return addresses.get(0).getCountryName() + "," + addresses.get(0).getLocality();
        }
        return null;
    }

    public static String getCountryNameByCoordinates(Context context, double lat, double lon) throws IOException {

        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addresses = geocoder.getFromLocation(lat, lon, 1);
        if (addresses != null && addresses.size() > 0) {
            return addresses.get(0).getCountryName();
        }
        return null;
    }

    public static String getCityNameByCoordinates(Context context, double lat, double lon) throws IOException {

        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        ;
        List<Address> addresses = geocoder.getFromLocation(lat, lon, 1);
        if (addresses != null && addresses.size() > 0) {
            return addresses.get(0).getLocality();
        }
        return null;
    }

    public static String getPhoneLocaleCountryCode(Context context) {
        String locale = "";
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            LocaleList list = context.getResources().getConfiguration().getLocales();
            if (list.size() > 0) {
                locale = list.get(0).getCountry();
            }
        } else {
            locale = context.getResources().getConfiguration().locale.getCountry();
        }

        return locale;
    }
}
