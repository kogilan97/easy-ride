package com.finalyearproject.easyride.easyride.utils;

import android.view.Gravity;
import android.view.View;

import com.mancj.slideup.SlideUp;
import com.mancj.slideup.SlideUpBuilder;

public class CustomizedSlideUpBuilder { View builderView,dimView;
    boolean slideDown;

    /**
     * <p>Construct a SlideUp by passing the view or his child to use for the generation</p>
     *
     * @param sliderView
     */
    public CustomizedSlideUpBuilder(View sliderView,View dimView) {
        builderView = sliderView;
        this.dimView = dimView;
    }

    public void setDimViewClicked(View.OnClickListener onClickListener) {
        dimView.setOnClickListener(onClickListener);
    }

    public SlideUp build(){
        SlideUpBuilder builder = new SlideUpBuilder(builderView)
                .withListeners(new SlideUp.Listener.Events() {
                    @Override
                    public void onSlide(float percent) {
                        slideDown = percent == 0.0;
                        dimView.setAlpha(1 - (percent / 100));
                    }

                    @Override
                    public void onVisibilityChanged(int visibility) {
                        if (visibility == View.GONE) {
                            // don't care
                        }
                    }
                })
                .withStartGravity(Gravity.BOTTOM)
                .withLoggingEnabled(true)
                .withGesturesEnabled(true)
                .withStartState(SlideUp.State.HIDDEN);

        return builder.build();
    }

    public SlideUp.Listener.Slide getSlideUpListener(){
        return new SlideUp.Listener.Slide() {
            @Override
            public void onSlide(float percent) {
                if (percent == 100) {
                    dimView.setVisibility(View.GONE);
                } else {
                    dimView.setVisibility(View.VISIBLE);
                }
            }
        };
    }
}
