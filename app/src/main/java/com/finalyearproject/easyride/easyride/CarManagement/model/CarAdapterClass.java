package com.finalyearproject.easyride.easyride.CarManagement.model;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.finalyearproject.easyride.R;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CarAdapterClass extends RecyclerView.Adapter<CarAdapterClass.CarHolder> {

    private Context context;
    private List<CarDetailsClass> cars;
    public CarAdapterClass.itemClickListener mCallback;

    public CarAdapterClass(Context context, List<CarDetailsClass> cars) {
        this.context = context;
        this.cars = cars;
    }

    @NonNull
    @Override
    public CarHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_layout, parent, false);
        return new CarHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CarHolder holder, int position) {

        CarDetailsClass carDetailsClass = cars.get(position);
        holder.txtcarnumber.setText("Vehicle Number : " + carDetailsClass.getCarnumber());
        holder.txtcapacity.setText("Capacity : " + carDetailsClass.getCapacity()+"pax");
        holder.txtcarmodel.setText( carDetailsClass.getCarmodel());

        holder.btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mCallback!=null){
                    mCallback.onItemClicked(carDetailsClass.getId());
                }
            }
        });

        if(carDetailsClass.getImage_file() != null)
            Glide
                .with(context)
                .load(Uri.parse(carDetailsClass.getImage_file()))
                .centerInside()
                .thumbnail(0.1f)
                .placeholder(R.drawable.round_bg)
                .into(holder.profile_image);
    }

    @Override
    public int getItemCount() {
        return cars.size();
    }


    public class CarHolder extends RecyclerView.ViewHolder {

        private TextView txtcarmodel;
        private TextView txtcarnumber;
        private TextView txtcapacity;
        private ImageView btn_close;
        private CircleImageView profile_image;

        public CarHolder(View itemView) {
            super(itemView);
            txtcarmodel = itemView.findViewById(R.id.txtCarModel);
            txtcapacity = itemView.findViewById(R.id.txtCarCapacity);
            txtcarnumber = itemView.findViewById(R.id.txtCarNumber);
            btn_close=itemView.findViewById(R.id.btn_close);
            profile_image = itemView.findViewById(R.id.profile_image);
        }
    }

    public void setOnItemClickListener(final itemClickListener listener){
        mCallback=listener;
    }

    public interface itemClickListener{
        void onItemClicked(String id);
    }
}
