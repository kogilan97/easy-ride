package com.finalyearproject.easyride.easyride.Payment.presenter;

import android.content.Context;
import android.util.Log;

import com.finalyearproject.easyride.EasyRideApplication;
import com.finalyearproject.easyride.data.UserModalClass;
import com.finalyearproject.easyride.database.EasyRideStorage;
import com.finalyearproject.easyride.di.module.APIInterface;
import com.finalyearproject.easyride.di.module.RetrofitModule;
import com.finalyearproject.easyride.easyride.Payment.view.PaymentActivity;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class PaymentPresenterImpl implements PaymentContract.Presenter {
    private APIInterface mApiInterface;
    private PaymentContract.View mView;
    PaymentActivity paymentActivity;

    @Inject
    public PaymentPresenterImpl(APIInterface mApiInterface, PaymentContract.View view) {
        this.mApiInterface = mApiInterface;
        this.mView = view;
    }

    @Override
    public void getCard() {
        mView.showProgress();
        mApiInterface.getCreditCard(EasyRideStorage.getInstance().getAuthToken(),EasyRideStorage.getInstance().getPrefUserId()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {
                    @Override
                    public void onCompleted() {
                        mView.showComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        // mView.showError(ErrorUtils.getError(e));
                    }

                    @Override
                    public void onNext(UserModalClass status) {
                        mView.hideProgress();
                        mView.onShowCard(status.getCard());
                    }
                });
    }

    @Override
    public void createPayment() {

    }

    @Override
    public void chargePassenger(String journeyId, String dateTime, String amount) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("journeyId", journeyId);
            jsonObject.put("dateTime", dateTime);
            jsonObject.put("amount", amount);
            jsonObject.put("userId", EasyRideStorage.getInstance().getPrefUserId());

        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        RequestBody body =
                RequestBody.create(MediaType.parse("raw"), jsonObject.toString());

        mApiInterface.makePayment(EasyRideStorage.getInstance().getAuthToken(), body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {
                    @Override
                    public void onCompleted() {
                        mView.showComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(UserModalClass status) {
                        mView.hideProgress();

                        if (!status.getStatus()) {
                            mView.showError(status.getMessage());

                        } else {
                            EasyRideStorage.getInstance().setCreditCardAdded(true);
                            mView.onNext(status.getMessage());
                        }


                    }
                });
    }

    @Override
    public void deleteCard() {
        mView.showProgress();
        mApiInterface.deleteCreditCard(EasyRideStorage.getInstance().getAuthToken(),EasyRideStorage.getInstance().getPrefUserId()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {
                    @Override
                    public void onCompleted() {
                        mView.showComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        // mView.showError(ErrorUtils.getError(e));
                    }

                    @Override
                    public void onNext(UserModalClass status) {
                        mView.hideProgress();
                        mView.onCardDeleted();
                    }
                });
    }

    @Override
    public void uploadCard(String number, String month, String year, String cvv, Context context) {
        mView.showProgress();
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("user_id", EasyRideStorage.getInstance().getPrefUserId());
            jsonObject.put("card_number", number);
            jsonObject.put("exp_month", Integer.valueOf(month));
            jsonObject.put("exp_year", Integer.valueOf(year));
            jsonObject.put("cvc", cvv);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        RequestBody body =
                RequestBody.create(MediaType.parse("raw"), jsonObject.toString());

        mApiInterface.createCreditCard(EasyRideStorage.getInstance().getAuthToken(), body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {
                    @Override
                    public void onCompleted() {
                        mView.showComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        // mView.hideProgress();
                        mView.showError(e.getMessage());
                        Log.e("ErrorCardUPDT",e.getLocalizedMessage());
                    }

                    @Override
                    public void onNext(UserModalClass status) {
                        mView.hideProgress();

                        if (!status.getStatus()) {
                            mView.showError(status.getMessage());

                        } else {
                            EasyRideStorage.getInstance().setCreditCardAdded(true);
                            mView.onNext(status.getMessage());
                        }


                    }
                });
    }
}
