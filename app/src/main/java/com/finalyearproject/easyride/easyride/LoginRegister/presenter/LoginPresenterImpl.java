package com.finalyearproject.easyride.easyride.LoginRegister.presenter;

import android.util.Log;
import android.widget.Toast;

import com.finalyearproject.easyride.EasyRideConstants;
import com.finalyearproject.easyride.database.EasyRideStorage;
import com.finalyearproject.easyride.di.module.APIInterface;
import com.finalyearproject.easyride.di.module.RetrofitModule;
import com.finalyearproject.easyride.data.UserModalClass;
import com.finalyearproject.easyride.di.qualifier.ApplicationContext;
import com.finalyearproject.easyride.easyride.LoginRegister.model.UserInfo;
import com.finalyearproject.easyride.easyride.LoginRegister.view.LoginActivity;
import com.finalyearproject.easyride.easyride.LoginRegister.view.SignupActivity;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.finalyearproject.easyride.EasyRideApplication.getContext;

public class LoginPresenterImpl implements LoginRegisterContract.Presenter{

    private SignupActivity signupActivity;
    private LoginActivity loginActivity;
    private APIInterface mAPIInterface;

    public LoginPresenterImpl(SignupActivity view) {
        this.signupActivity = view;
        mAPIInterface = RetrofitModule.getApiInterface();
    }

    public LoginPresenterImpl(LoginActivity view) {
        this.loginActivity = view;
        mAPIInterface = RetrofitModule.getApiInterface();
    }
    @Override
    public void onFetchDetails(String username, String password, String email, String PhoneNumber) {

        JSONObject object = new JSONObject();
        try {
            object.put("username", username);
            object.put("password", password);
            object.put("emailaddress", email);
            object.put("phonenumber", PhoneNumber);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("raw"),object.toString());
        mAPIInterface.getUserRegistration(body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {
                    @Override
                    public void onCompleted() {

                    }
                    @Override
                    public void onError(Throwable e) {
                        signupActivity.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(UserModalClass userModalClass) {
                        signupActivity.onNext(userModalClass.getMessage(),userModalClass.getStatus());
                    }
                });

    }

    private void storeUserBasicInfo(final UserInfo info){
        EasyRideStorage.getInstance().setPrefUsername(info.getUsername());
        EasyRideStorage.getInstance().setPrefEmailAddr(info.getEmail());
        EasyRideStorage.getInstance().setPrefAppRole(info.getApprole());
        EasyRideStorage.getInstance().setPrefUserId(info.getId());
    }


    @Override
    public void onFetchLoginDetails(String email, String password) {

        JSONObject object = new JSONObject();
        try {
            object.put("emailaddress", email);
            object.put("password", password);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("raw"),object.toString());
        mAPIInterface.getUserLoginDetails(body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        loginActivity.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(UserModalClass userModalClass) {

                        if(userModalClass.getStatus())
                        {
                            storeUserBasicInfo(userModalClass.getUserInfo());
                            EasyRideStorage.getInstance().setPrefLoggedIn(true);
                            EasyRideStorage.getInstance().setAuthToken(userModalClass.getToken());
                            EasyRideStorage.getInstance().setCurrentRole(EasyRideConstants.ROLE_USER);
                            loginActivity.onNext(userModalClass.getMessage(),userModalClass.getStatus());
                        }
                        else
                        {
                            Toast.makeText(getContext(),userModalClass.getMessage(),Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }
}
