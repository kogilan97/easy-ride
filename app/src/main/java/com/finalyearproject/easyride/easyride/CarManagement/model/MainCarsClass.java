package com.finalyearproject.easyride.easyride.CarManagement.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MainCarsClass {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("cardetails")
    @Expose
    private List<CarDetailsClass> cardetails = null;

    @SerializedName("product")
    private CarDetailsClass carDetailsClass;

    public CarDetailsClass getCarDetailsClass() {
        return carDetailsClass;
    }

    public void setCarDetailsClass(CarDetailsClass carDetailsClass) {
        this.carDetailsClass = carDetailsClass;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public  String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CarDetailsClass> getCardetails() {
        return cardetails;
    }

    public void setCardetails(List<CarDetailsClass> cardetails) {
        this.cardetails = cardetails;
    }

}
