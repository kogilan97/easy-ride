package com.finalyearproject.easyride.easyride.LoginRegister.view;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.finalyearproject.easyride.R;
import com.finalyearproject.easyride.easyride.LoginRegister.presenter.LoginPresenterImpl;
import com.finalyearproject.easyride.easyride.LoginRegister.presenter.LoginRegisterContract;
import com.finalyearproject.easyride.easyride.view.BaseActivity;
import com.finalyearproject.easyride.easyride.view.RiderMainActivity;


public class LoginActivity extends BaseActivity implements LoginRegisterContract.View {

    private TextView signuptext;
    private Button btnlogin;
    private EditText txtemail;
    private EditText txtpassword;

    LoginPresenterImpl mPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mPresenter = new LoginPresenterImpl(this);
        signuptext = findViewById(R.id.txtsignup);
        txtemail = findViewById(R.id.input_email);
        txtpassword = findViewById(R.id.input_password);

        signuptext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, SignupActivity.class));
            }
        });

        //to open ridermain activity
        btnlogin = (Button) findViewById(R.id.btnlogin);
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CannotBeEmpty()) {
                    if (ValidateEmail(txtemail.getText()))
                        SaveLoginDetails();
                    else
                        Toast.makeText(getApplicationContext(), "Invalid Email Format", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(getApplicationContext(), "Email Address or Password cannot be empty", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public boolean ValidateEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public boolean CannotBeEmpty() {
        return (!TextUtils.isEmpty(txtemail.getText()) && !TextUtils.isEmpty(txtpassword.getText()));
    }

    public void SaveLoginDetails() {
        mPresenter.onFetchLoginDetails(txtemail.getText().toString(), txtpassword.getText().toString());
    }

    @Override
    public void onNext(String msg, Boolean status) {

        if (status) {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
            startActivity(new Intent(LoginActivity.this, RiderMainActivity.class));
        } else {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        }}

    @Override
    public void showError(String msg) {
        Log.d("TAG1", msg);
    }
}
