package com.finalyearproject.easyride.easyride.BookJourney.presenter;

import com.finalyearproject.easyride.easyride.BookJourney.model.Path;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.Date;
import java.util.List;

public interface BookJourneyContract {
    interface View {
        void onShowPathList(String message, List<Path> path);

        void onShowPathDetails(String message, Path path, Marker marker, String price);

        void onNext(String message);

        void showError(String message);

        void showComplete();

        void showProgress();

        void hideProgress();


    }

    interface Presenter {
        void getPath(String id, Marker marker, String price);

        void searchPath(LatLng start, LatLng end, String date);

        void createJourney(String formattedDateForBackend, String carId, String driverId, double startLatitude, double startLongitude, double endLatitude, double endLongitude);

    }
}
