package com.finalyearproject.easyride.easyride.Payment.di.module;

import android.content.Context;

import com.finalyearproject.easyride.di.qualifier.ActivityContext;
import com.finalyearproject.easyride.di.scope.ActivityScope;
import com.finalyearproject.easyride.easyride.Payment.view.ActiveCardActivity;
import com.finalyearproject.easyride.easyride.Payment.view.AddCardActivity;
import com.finalyearproject.easyride.easyride.Payment.view.PaymentActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class PaymentContextModule {

    private AddCardActivity mAddCardActivity;
    private ActiveCardActivity mActiveCardActivity;
    private PaymentActivity mPaymentActivity;

    public Context mContext;



    public PaymentContextModule(AddCardActivity activity){
        this.mAddCardActivity = activity;
    }

    public PaymentContextModule(ActiveCardActivity activity){
        this.mActiveCardActivity = activity;
    }

    public PaymentContextModule(PaymentActivity activity){
        this.mPaymentActivity = activity;
    }

    @Provides
    @ActivityScope
    public AddCardActivity provideAddCardActivity(){
        return mAddCardActivity;
    }

    @Provides
    @ActivityScope
    public ActiveCardActivity provideActiveCardActivity(){
        return mActiveCardActivity;
    }

    @Provides
    @ActivityScope
    public PaymentActivity providePaymentActivity(){
        return mPaymentActivity;
    }


    @Provides
    @ActivityScope
    @ActivityContext
    public Context provideContext(){
        return mContext;
    }
}
