package com.finalyearproject.easyride.easyride.view;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import com.finalyearproject.easyride.EasyRideConstants;
import com.finalyearproject.easyride.R;
import com.finalyearproject.easyride.SwitchRoleActivity;
import com.finalyearproject.easyride.easyride.EditProfile.view.RiderProfileFragment;
import com.finalyearproject.easyride.easyride.Feedback.RiderContactUsFragment;
import com.finalyearproject.easyride.easyride.Journey.view.JourneyFragment;
import com.finalyearproject.easyride.easyride.utils.Navigator;
import com.google.android.material.bottomnavigation.BottomNavigationView;

//implement the interface OnNavigationItemSelectedListener in your activity class
public class RiderMainActivity extends BaseActivity implements SwitchRoleActivity.OnSwitchRoleListener {

    RiderHomeFragment riderHomeFragment = RiderHomeFragment.getInstance();
    JourneyFragment journeyFragment = JourneyFragment.getInstance();
    RiderProfileFragment riderProfileFragment = RiderProfileFragment.getInstance();
    RiderContactUsFragment riderContactUsFragment = RiderContactUsFragment.getInstance();


    private Navigator mNavigator;

    private BottomNavigationView.OnNavigationItemSelectedListener mUserNavigationListener = menuItem -> {
        switch (menuItem.getItemId()) {
            case R.id.navigation_home:
                replaceFragment(riderHomeFragment);
                return true;

            case R.id.navigation_journey:
                replaceFragment(journeyFragment);
                return true;

            case R.id.navigation_profile:
                replaceFragment(riderProfileFragment);
                return true;

            case R.id.navigation_contact_us:
                replaceFragment(riderContactUsFragment);
                return true;


        }
        return false;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rider_main);
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(mUserNavigationListener);
        initNavigator();
        SwitchRoleActivity.setOnFinishActivityListener(this);
    }

    private void initNavigator() {
        if (mNavigator != null) {
            return;
        }
        mNavigator = new Navigator(getSafeFragmentManager());
        mNavigator.setRootFragment(riderHomeFragment);
    }

    private void replaceFragment(Fragment fragment) {
        if (mNavigator != null) {
            mNavigator.replaceFragment(fragment);
        }
    }

    @Override
    public void onSwitchRole(int role) {
        if (role == EasyRideConstants.ROLE_DRIVER) {
            startActivity(new Intent(this, DriverMainActivity.class));
            finish();
        }
    }
}
