package com.finalyearproject.easyride.easyride.Payment.presenter;

public interface PaymentCompleteContract {
    interface View{

        void onNext(String message);

        void showError(String message);

        void showComplete();

        void showProgress();

        void hideProgress();
    }
}
