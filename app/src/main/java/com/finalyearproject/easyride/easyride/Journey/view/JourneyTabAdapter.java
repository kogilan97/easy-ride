package com.finalyearproject.easyride.easyride.Journey.view;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class JourneyTabAdapter extends FragmentPagerAdapter {
    private static final String[] TITLES = new String[]{
           "Upcoming","Current","Past"
    };

    private static final int NUM_TITLES = TITLES.length;

    JourneyTabAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return UpcomingJourneyFragment.getInstance();
            case 1:
                return CurrentJourneyFragment.getInstance();
            case 2:
                return PastJourneyFragment.getInstance();
            default:
                return UpcomingJourneyFragment.getInstance();
        }
    }

    @Override
    public int getCount() {
        return NUM_TITLES;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }
}