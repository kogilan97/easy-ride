package com.finalyearproject.easyride.easyride.Driver.view;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.finalyearproject.easyride.R;
import com.finalyearproject.easyride.database.EasyRideStorage;
import com.finalyearproject.easyride.easyride.BookJourney.view.LocationSearchAdapter;
import com.finalyearproject.easyride.easyride.Driver.model.PathInfo;
import com.finalyearproject.easyride.easyride.Driver.presenter.PathPresenterImpl;
import com.finalyearproject.easyride.easyride.utils.ClearableEditText;
import com.finalyearproject.easyride.easyride.utils.CustomizedSlideUpBuilder;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.mancj.slideup.SlideUp;
import com.skyfishjy.library.RippleBackground;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class DriverHomeFragment extends Fragment implements LocationSearchAdapter.ClickListener,PathAdapter.PathItemClickListener {

    private RelativeLayout relativeLayout;
    private SlideUp mAddressSlider;

    @BindView(R.id.dim)
    View mDimView;
    @BindView(R.id.search_location_view)
    View mAddressSliderView;
    @BindView(R.id.txt_name)
    TextView txt_name;
    @BindView(R.id.path_recycler_view)
    RecyclerView path_recycler_view;

    private Unbinder mUnBinder;

    // filter by search
    public PlacesClient mPlacesClient;
    private LocationSearchAdapter mLocationSearchAdapter;
    private static LatLngBounds BOUNDS_WORLD = new LatLngBounds(new LatLng(-85, 180), new LatLng(85, -180));
    private RecyclerView location_recycler;

    private PathAdapter pathAdapter;
    private PathPresenterImpl pathPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_driver_home, container, false);
        mUnBinder = ButterKnife.bind(this, view);
        pathPresenter = new PathPresenterImpl(this);
        relativeLayout = view.findViewById(R.id.content);
        Button btnStart = (Button) view.findViewById(R.id.btn_start);
        ClearableEditText startSearchEditText = mAddressSliderView.findViewById(R.id.startSearchEditText);
        ClearableEditText endSearchEditText = mAddressSliderView.findViewById(R.id.endSearchEditText);


        Places.initialize(this.getContext(), getString(R.string.google_places_api_key));
        mPlacesClient = Places.createClient(getContext());
        txt_name.setText("Hello " + EasyRideStorage.getInstance().getPrefUsername() + " !");
        initSliderLocation();

        mLocationSearchAdapter = new LocationSearchAdapter(getContext());
        mLocationSearchAdapter.setClickListener(this);
        location_recycler.setAdapter(mLocationSearchAdapter);
        mLocationSearchAdapter.notifyDataSetChanged();

        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAddressSlider.show();
            }
        });


        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AddPathActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable("start", getLocationFromAddress(startSearchEditText.getText().toString()));
                bundle.putParcelable("end", getLocationFromAddress(endSearchEditText.getText().toString()));
                intent.putExtras(bundle);
                startActivity(intent);

                closeSliderUp(view, mAddressSlider);
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        pathPresenter.onFetchDetails();
    }

    public void onPathFetched(List<PathInfo> pathInfoList){
        pathAdapter = new PathAdapter(getContext(),pathInfoList);
        pathAdapter.setOnItemClickListener(this);
        path_recycler_view.setAdapter(pathAdapter);
        pathAdapter.notifyDataSetChanged();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Driver Home");
    }

    private void initSliderLocation() {
        CustomizedSlideUpBuilder slideUpBuilder = new CustomizedSlideUpBuilder(mAddressSliderView, mDimView);
        // slideUpBuilder.setDimViewClicked(v -> closeSliderUp(v, mAddressSlider));
        mAddressSlider = slideUpBuilder.build();
        mAddressSlider.addSlideListener(slideUpBuilder.getSlideUpListener());

        TextView closeBtn = mAddressSliderView.findViewById(R.id.close_btn);
        closeBtn.setOnClickListener(v -> closeSliderUp(v, mAddressSlider));

        location_recycler = mAddressSliderView.findViewById(R.id.location_recycler);
        location_recycler.setLayoutManager(new LinearLayoutManager(getContext()));

        ClearableEditText startSearchEditText = mAddressSliderView.findViewById(R.id.startSearchEditText);
        ClearableEditText endSearchEditText = mAddressSliderView.findViewById(R.id.endSearchEditText);

        startSearchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager in = (InputMethodManager) DriverHomeFragment.this.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(DriverHomeFragment.this.getActivity().getWindow().getDecorView().getRootView().getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        endSearchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager in = (InputMethodManager) DriverHomeFragment.this.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(DriverHomeFragment.this.getActivity().getWindow().getDecorView().getRootView().getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        startSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (mLocationSearchAdapter == null) {
                    return;
                }
                if (start < 4) {
                    mLocationSearchAdapter.notifyDataSetChanged();
                    return;
                }
                if (mLocationSearchAdapter != null) {
                    if (!s.toString().equals("")) {
                        mLocationSearchAdapter.getFilter().filter(s.toString());
                        if (location_recycler.getVisibility() == View.GONE) {
                            location_recycler.setVisibility(View.VISIBLE);
                        }
                    } else {
                        if (location_recycler.getVisibility() == View.VISIBLE) {
                            location_recycler.setVisibility(View.GONE);
                        }
                    }
                }

                if (count == 0) {
                    if (mLocationSearchAdapter != null) {
                        mLocationSearchAdapter.clear();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (startSearchEditText.getText().toString().isEmpty()) {
                    if (mLocationSearchAdapter != null) {
                        mLocationSearchAdapter.clear();
                    }
                }
            }
        });


        endSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (mLocationSearchAdapter == null) {
                    return;
                }
                if (start < 4) {
                    mLocationSearchAdapter.notifyDataSetChanged();
                    return;
                }
                if (mLocationSearchAdapter != null) {
                    if (!s.toString().equals("")) {
                        mLocationSearchAdapter.getFilter().filter(s.toString());
                        if (location_recycler.getVisibility() == View.GONE) {
                            location_recycler.setVisibility(View.VISIBLE);
                        }
                    } else {
                        if (location_recycler.getVisibility() == View.VISIBLE) {
                            location_recycler.setVisibility(View.GONE);
                        }
                    }
                }

                if (count == 0) {
                    if (mLocationSearchAdapter != null) {
                        mLocationSearchAdapter.clear();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (endSearchEditText.getText().toString().isEmpty()) {
                    if (mLocationSearchAdapter != null) {
                        mLocationSearchAdapter.clear();
                    }
                }
            }
        });

        TextView txtMyLocation = mAddressSliderView.findViewById(R.id.txtMyLocation);
        txtMyLocation.setOnClickListener(v -> {
            closeSliderUp(v, mAddressSlider);
        });

    }

    private void closeSliderUp(View view, SlideUp slideUp) {
        if (slideUp.isVisible()) {

            slideUp.hide();

            //slideUp.hide();
        }
       /* InputMethodManager in = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view.getWindowToken(), 0);*/
    }


    @OnClick(R.id.dim)
    public void onDimClicked(View view) {
        if (mAddressSlider.isVisible()) {

            mAddressSlider.hide();

        }

//        if (mCalendarSlider.isVisible()) {
//            mCalendarSlider.hide();
//        }
    }

    public LatLng getLocationFromAddress(String strAddress) {

        Geocoder coder = new Geocoder(this.getContext());
        List<Address> address;
        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude());

            return p1;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return p1;
    }

    @Override
    public void click(Place place) {
        ClearableEditText startSearchEditText = mAddressSliderView.findViewById(R.id.startSearchEditText);
        ClearableEditText endSearchEditText = mAddressSliderView.findViewById(R.id.endSearchEditText);
        if (startSearchEditText.isFocused())
            startSearchEditText.setText(place.getAddress());
        else if (endSearchEditText.isFocused())
            endSearchEditText.setText(place.getAddress());
    }

    @Override
    public void onItemClick(String journeyID) {

    }
}
