package com.finalyearproject.easyride.easyride.CarManagement.presenter;

import android.net.Uri;

import com.finalyearproject.easyride.easyride.CarManagement.model.CarDetailsClass;

import java.util.List;

public interface CarsInterface {

    interface Presenter{
        void onFetchDetails();
        void OnAddCarDetails(String carplateno, String carmodel, String pax, Uri uri);
        void onDeleteCar(String id);
    }


    interface View {
        void onNext(String msg, List<CarDetailsClass> CarDetailsClass);
        void showError(String msg);
    }

    interface AddCarView {
        void onNext(String msg, Boolean status);
        void showError(String msg);
    }

}
