package com.finalyearproject.easyride.easyride.BookJourney.view;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.finalyearproject.easyride.R;
import com.finalyearproject.easyride.easyride.BookJourney.model.Path;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import java.util.HashMap;

class InfoWindowCustom implements GoogleMap.InfoWindowAdapter {
    Context mContext;
    LayoutInflater inflater;
    ImageView pic;
    TextView txtName;
    TextView txtPrice;
    TextView txtTrips;
    RatingBar ratingBar;
    HashMap<String, Path> mMarkers;
    Path path;
    String price;

    public InfoWindowCustom(Context context, HashMap<String, Path> markers, Path path, String price) {
        mContext = context;
        mMarkers = markers;
        this.path = path;
        this.price = price;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        String markerId = marker.getId();
        if (mMarkers == null || !mMarkers.containsKey(markerId)) {
            return null;
        }

        Path path = mMarkers.get(markerId);

        View v = inflater.inflate(R.layout.layout_info_maker, null);
        pic = v.findViewById(R.id.pic);
        txtName = v.findViewById(R.id.txtName);
        txtPrice = v.findViewById(R.id.txtPrice);


        txtName.setText(this.path.getDriverName());
        txtPrice.setText("RM" +this.price);
        return v;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }
}
