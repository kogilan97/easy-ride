package com.finalyearproject.easyride.easyride.Journey.presenter;

import com.finalyearproject.easyride.easyride.Journey.model.JourneyInfo;

import java.util.List;

public interface JourneyContract {

    interface View{
        void onNext(String message);

        void showError(String message);

        void onShowJourneyList(List<JourneyInfo> journeyInfoList);

        void showComplete();

        void showProgress();

        void hideProgress();
    }

    interface Presenter{
        void getJourneyList(String status);

        void getJourneyDetails(String journeyID);
    }
}
