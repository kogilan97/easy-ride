package com.finalyearproject.easyride.easyride.Earnings.presenter;

import com.finalyearproject.easyride.data.UserModalClass;
import com.finalyearproject.easyride.database.EasyRideStorage;
import com.finalyearproject.easyride.di.module.APIInterface;
import com.finalyearproject.easyride.di.module.RetrofitModule;
import com.finalyearproject.easyride.easyride.Earnings.view.DriverEarningFragment;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;



public class earningPresenterImpl {

    DriverEarningFragment driverEarningFragment;
    APIInterface mAPIInterface;

    public earningPresenterImpl(DriverEarningFragment view) {
        this.driverEarningFragment = view;
        mAPIInterface = RetrofitModule.getApiInterface();
    }



    public void onFetchDetails() {
        mAPIInterface.viewEarnings(EasyRideStorage.getInstance().getAuthToken(),EasyRideStorage.getInstance().getPrefUserId()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        driverEarningFragment.showError(e.toString());
                    }

                    @Override
                    public void onNext(UserModalClass userModalClass) {
                        driverEarningFragment.onNext(userModalClass.getMessage(),userModalClass.getTotalAmount(), userModalClass.getEarningInfo());
                    }
                });
    }
}
