package com.finalyearproject.easyride.easyride.Earnings.view;

import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.finalyearproject.easyride.R;
import com.finalyearproject.easyride.easyride.CarManagement.model.CarAdapterClass;
import com.finalyearproject.easyride.easyride.CarManagement.model.CarDetailsClass;
import com.finalyearproject.easyride.easyride.Earnings.model.EarningInfo;
import com.finalyearproject.easyride.easyride.Earnings.presenter.earningPresenterImpl;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class DriverEarningFragment extends Fragment {
    private Unbinder mUnBinder;

    EarningsAdapter earningsAdapter;

    @BindView(R.id.txtTotalAmount)
    TextView txtTotalAmount;
    @BindView(R.id.earning_recycler_view)
    RecyclerView earning_recycler_view;

    earningPresenterImpl earningPresenter;

    @Nullable
    @Override

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_driver_earning, container, false);
        mUnBinder = ButterKnife.bind(this, view);
        earningPresenter = new earningPresenterImpl(this);
        return view;
    }
    public void onStart() {
        super.onStart();
        earningPresenter.onFetchDetails();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

  public void onNext(String msg,String totalAmount, List<EarningInfo> earningInfo)
    {
        if(msg.equals("No Earning Details"))
        {
            txtTotalAmount.setText("Zero Earnings");
        }
        else
        {
            txtTotalAmount.setText("Total Earnings: RM " + totalAmount);
            earning_recycler_view.setHasFixedSize(true);
            earning_recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
            earningsAdapter = new EarningsAdapter(getActivity(), earningInfo);
            earning_recycler_view.setAdapter(earningsAdapter);
        }

    }
   public void showError(String msg)
    {
        Toast.makeText(getContext(),msg,Toast.LENGTH_SHORT).show();
    }
}
