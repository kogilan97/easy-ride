package com.finalyearproject.easyride.easyride.EditProfile.view;

import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.finalyearproject.easyride.R;
import com.finalyearproject.easyride.easyride.EditProfile.presenter.EditProfilePresenterImpl;
import com.finalyearproject.easyride.easyride.view.BaseActivity;

public class VerifyPhoneActivity extends BaseActivity {

    String id;
    EditProfilePresenterImpl mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_phone);
        mPresenter=new EditProfilePresenterImpl(this);

        EditText pin_code_1 = findViewById(R.id.pin_code_1);
        EditText pin_code_2 = findViewById(R.id.pin_code_2);
        EditText pin_code_3 = findViewById(R.id.pin_code_3);
        EditText pin_code_4 = findViewById(R.id.pin_code_4);
        TextView pincode_next_btn = findViewById(R.id.pincode_next_btn);

        id = getIntent().getExtras().equals(null) ? null : getIntent().getExtras().getString("id");

        pincode_next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!pin_code_1.getText().toString().equals("")&&!pin_code_2.getText().toString().equals("")&&!pin_code_3.getText().toString().equals("")&&!pin_code_3.getText().toString().equals(""))
                {
                    if (id != null) {
                        String code = pin_code_1.getText().toString() + pin_code_2.getText().toString() + pin_code_3.getText().toString() + pin_code_4.getText().toString();
                        mPresenter.placeVerification(id, code);
                    }

                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Text fields cannot be empty", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void onSuccess(String msg){
        if(msg.equals(""))
        {
            Toast.makeText(this, "Incorrect Pin Number",Toast.LENGTH_SHORT).show();
            finish();
        }
        else
        {
            Toast.makeText(this, msg,Toast.LENGTH_SHORT).show();
            finish();
        }


    }
}
