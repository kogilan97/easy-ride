package com.finalyearproject.easyride.easyride.EditProfile.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.finalyearproject.easyride.EasyRideConstants;
import com.finalyearproject.easyride.R;
import com.finalyearproject.easyride.SwitchRoleActivity;
import com.finalyearproject.easyride.database.EasyRideStorage;
import com.finalyearproject.easyride.easyride.EditProfile.presenter.EditProfileInterface;
import com.finalyearproject.easyride.easyride.EditProfile.presenter.EditProfilePresenterImpl;
import com.finalyearproject.easyride.easyride.LoginRegister.model.UserInfo;
import com.finalyearproject.easyride.easyride.LoginRegister.view.LoginActivity;
import com.finalyearproject.easyride.easyride.Payment.view.ActiveCardActivity;
import com.finalyearproject.easyride.easyride.view.DriverMainActivity;


public class RiderProfileFragment extends Fragment implements EditProfileInterface.View {

    private LinearLayout lbleditprofile;
    private LinearLayout lblpayment;
    private LinearLayout lbllogout;
    private LinearLayout lblemergencycontact;
    private LinearLayout lblswitchtodriver;
    private TextView txt_name,txt_switch;

    EditProfilePresenterImpl mPresenter;

    public static RiderProfileFragment getInstance() {
        return new RiderProfileFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rider_profile, container, false);

        LinearLayout layout = view.findViewById(R.id.logout_container);
        lblpayment = view.findViewById(R.id.payment_container);
        mPresenter = new EditProfilePresenterImpl(this);

        lblpayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RiderProfileFragment.this.getActivity(), ActiveCardActivity.class));
            }
        });

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Activity activity = RiderProfileFragment.this.getActivity();
                startActivity(new Intent(RiderProfileFragment.this.getActivity(),LoginActivity.class));
                if(activity!=null)
                    activity.finish();

                //clear shared preferences
                EasyRideStorage.getInstance().setAuthToken(null);
                EasyRideStorage.getInstance().setPrefLoggedIn(false);
                EasyRideStorage.getInstance().setPrefAppRole(null);
                EasyRideStorage.getInstance().setPrefEmailAddr(null);
                EasyRideStorage.getInstance().setPrefUsername(null);
                EasyRideStorage.getInstance().setPrefUserId(null);
            }
        });

        // Edit Profile Page
        lbleditprofile = view.findViewById(R.id.lbleditprofile);
        lbleditprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), RiderEditProfileActivity.class);
                startActivity(intent);
            }
        });

        //Switch to Driver Activity
        txt_switch = view.findViewById(R.id.txt_switch);
        lblswitchtodriver = view.findViewById(R.id.lblswitchtodriver);
        txt_name = view.findViewById(R.id.txt_name);

        txt_switch.setText(EasyRideStorage.getInstance().getCurrentRole() == EasyRideConstants.ROLE_USER ? "Switch to Driver" : "Switch to Passenger");


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.checkVerification();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Rider Profile");
    }

    @Override
    public void onNext(String msg, Boolean status, UserInfo userInfoClass) {
        txt_name.setText(userInfoClass.getUsername());
        lblswitchtodriver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(userInfoClass.getEmailverification() && userInfoClass.getPhoneverification() && userInfoClass.getICverification() && userInfoClass.getLicenseverification()){
                    startActivity(new Intent(getContext(), SwitchRoleActivity.class));
                }else{
                    Toast.makeText(RiderProfileFragment.this.getContext(),"Please complete your all verification first.",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void showError(String msg) {

    }
}
