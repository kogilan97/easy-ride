package com.finalyearproject.easyride.easyride.Journey.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.bumptech.glide.Glide;
import com.finalyearproject.easyride.R;
import com.finalyearproject.easyride.database.EasyRideStorage;
import com.finalyearproject.easyride.easyride.Journey.model.JourneyInfo;
import com.finalyearproject.easyride.easyride.Journey.presenter.JourneyPresenterImpl;
import com.finalyearproject.easyride.easyride.Payment.view.ActiveCardActivity;
import com.finalyearproject.easyride.easyride.Payment.view.PaymentActivity;
import com.finalyearproject.easyride.easyride.view.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.http.Body;

public class DriverJourneyDetails extends BaseActivity {

    @BindView(R.id.txt_passenger_name)
    TextView txt_passenger_name;
    @BindView(R.id.txt_phone_number)
    TextView txt_phone_number;
    @BindView(R.id.btn_phone)
    ImageView btn_phone;
    @BindView(R.id.btn_request_navigation)
    Button btn_request_navigation;
    @BindView(R.id.txt_total_payable)
    TextView txt_total_payable;
    @BindView(R.id.txt_total_earning)
    TextView txt_total_earning;
    @BindView(R.id.txt_fees)
    TextView txt_fees;
    @BindView(R.id.btn_pending_container)
    LinearLayout btn_pending_container;
    @BindView(R.id.btn_reject_journey)
    Button btn_reject_journey;
    @BindView(R.id.btn_accept_journey)
    Button btn_accept_journey;
    @BindView(R.id.btn_complete_journey)
    Button btn_complete_journey;
    @BindView(R.id.btn_start_journey)
    Button btn_start_journey;

    @BindView(R.id.static_map)
    ImageView static_map;
    @BindView(R.id.status)
    TextView status;
    @BindView(R.id.img_passenger)
    ImageView passengerImage;

    Button btnRequestNavigation;

    JourneyPresenterImpl mPresenter;
    String journeyID;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_journey_details);
        ButterKnife.bind(this);
        btnRequestNavigation = findViewById(R.id.btn_request_navigation);
        mPresenter = new JourneyPresenterImpl(this);
        journeyID = getIntent().getExtras().equals(null) ? null : getIntent().getExtras().getString("journeyID");


    }

    @Override
    protected void onStart() {
        super.onStart();
        if (journeyID != null)
            mPresenter.getDriverJourneyDetails(journeyID);
    }

    public void onGetDetails(JourneyInfo journeyInfo) {
        txt_passenger_name.setText(journeyInfo.getUserInfo().getUsername());
        txt_phone_number.setText(journeyInfo.getUserInfo().getPhonenumber());
        txt_total_payable.setText("RM "+journeyInfo.getPaymentInfo().getTotalAmount());
        txt_total_earning.setText("RM "+journeyInfo.getPaymentInfo().getEarned());
        txt_fees.setText("RM "+journeyInfo.getPaymentInfo().getCommission());

        btnRequestNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("tag2","entering");
                String uri = "http://maps.google.com/maps?saddr=" + journeyInfo.startPointLat + "," + journeyInfo.startPointLong + "&daddr=" + journeyInfo.endPointLat + "," + journeyInfo.endPointLong;
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
            }
        });


        btn_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(DriverJourneyDetails.this);
                builder.setTitle("Select Option");
                String[] items = new String[]{"Call", "Whatsapp"};
                builder.setItems(items, (dialog, which) -> {
                    if (which == 1) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=+6" + journeyInfo.getUserInfo().getPhonenumber()));
                        startActivity(intent);
                    }
                    if (which == 0) {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:" + journeyInfo.getUserInfo().getPhonenumber()));
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });
                builder.show();
            }
        });

        Glide
                .with(this)
                .load(Uri.parse("https://car-images-easyride.s3.amazonaws.com/uploadImage/kogi.jpg"))
                .fitCenter()
                .thumbnail(0.1f)
                .into(passengerImage);


        Glide
                .with(this)
                .load(Uri.parse("https://maps.googleapis.com/maps/api/staticmap?autoscale=2&size=1920x1080&maptype=roadmap&key=" + getString(R.string.google_places_api_key) + "&format=png&visual_refresh=true&markers=size:mid%7Ccolor:0xff0000%7Clabel:A%7C" + journeyInfo.getStartPointLat() + "," + journeyInfo.getStartPointLong() + "&markers=size:mid%7Ccolor:0xff0000%7Clabel:B%7C" + journeyInfo.getEndPointLat() + "," + journeyInfo.getEndPointLong()))
                .fitCenter()
                .thumbnail(0.1f)
                .into(static_map);

        btn_complete_journey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.completeJourney(journeyInfo.get_id());
            }
        });

        btn_start_journey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.startJourney(journeyInfo.get_id());
            }
        });

        btn_accept_journey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.acceptJourney(journeyInfo.get_id());
            }
        });

        btn_reject_journey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.rejectJourney(journeyInfo.get_id());
            }
        });

        switch (journeyInfo.getDriverResponse()) {
            case "Pending":
                btn_pending_container.setVisibility(View.VISIBLE);
                btn_start_journey.setVisibility(View.GONE);
                btn_complete_journey.setVisibility(View.GONE);
                status.setText("Pending");
                status.setBackground(getResources().getDrawable(R.drawable.bg_pending));
                break;
            case "Accepted":
                btn_pending_container.setVisibility(View.GONE);
                btn_start_journey.setVisibility(View.VISIBLE);
                btn_complete_journey.setVisibility(View.GONE);
                status.setText("Accepted");
                status.setBackground(getResources().getDrawable(R.drawable.bg_accepted));
                break;

            case "Paid":
                btn_pending_container.setVisibility(View.GONE);
                btn_start_journey.setVisibility(View.GONE);
                btn_complete_journey.setVisibility(View.VISIBLE);
                status.setText("Paid");
                status.setBackground(getResources().getDrawable(R.drawable.bg_started));
                break;
            case "Started":
                btn_pending_container.setVisibility(View.GONE);
                btn_start_journey.setVisibility(View.GONE);
                btn_complete_journey.setVisibility(View.VISIBLE);
                status.setText("Started");
                status.setBackground(getResources().getDrawable(R.drawable.bg_started));
                break;

            case "Declined":
                btn_pending_container.setVisibility(View.GONE);
                btn_start_journey.setVisibility(View.GONE);
                btn_complete_journey.setVisibility(View.GONE);
                status.setText("Declined");
                status.setBackground(getResources().getDrawable(R.drawable.bg_declined));
                break;
            case "Completed":
                btn_pending_container.setVisibility(View.GONE);
                btn_start_journey.setVisibility(View.GONE);
                btn_complete_journey.setVisibility(View.GONE);
                status.setText("Completed");
                status.setBackground(getResources().getDrawable(R.drawable.bg_completed));
                break;
        }
    }

    public void onNext(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        finish();
    }
}
