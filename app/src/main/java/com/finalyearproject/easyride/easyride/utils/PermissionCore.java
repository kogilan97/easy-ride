package com.finalyearproject.easyride.easyride.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.provider.Settings;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class PermissionCore {
    public static int RC_SIGN_IN = 355;
    public static int RC_GPS_ON = 93;

    public static void checkAllPermissions(Context context, Activity activity)
    {
        // location service permissions
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},2);
        }

        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.CAMERA},3);

            return;
        }

        //Add by Ridzuan 28/03/2017
        //Request to Read and Write image
        // location service permissions
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},4);
        }
    }

    public static boolean isGPSAvailable(Context context) throws Settings.SettingNotFoundException {
        int off = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
        return off==0?false:true;
    }

    public static void checkGPS(Context context,Activity activity) throws Settings.SettingNotFoundException {
        int off = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
        //return off==0?false:true;
        if(off==0){
            if(GpsCore.canToggleGPS()){
                GpsCore.turnGPSOn();
            }else {
                Intent onGPS = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                activity.startActivityForResult(onGPS, RC_GPS_ON);
            }
        }
    }

}
