package com.finalyearproject.easyride.easyride.Payment.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.biometric.BiometricPrompt;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.finalyearproject.easyride.EasyRideApplication;
import com.finalyearproject.easyride.R;
import com.finalyearproject.easyride.di.component.ApplicationComponent;
import com.finalyearproject.easyride.easyride.Payment.di.component.DaggerPaymentComponent;
import com.finalyearproject.easyride.easyride.Payment.di.component.PaymentComponent;
import com.finalyearproject.easyride.easyride.Payment.di.module.PaymentContextModule;
import com.finalyearproject.easyride.easyride.Payment.di.module.PaymentPresenterModule;
import com.finalyearproject.easyride.easyride.Payment.model.Card;
import com.finalyearproject.easyride.easyride.Payment.presenter.PaymentCompleteContract;
import com.finalyearproject.easyride.easyride.Payment.presenter.PaymentContract;
import com.finalyearproject.easyride.easyride.Payment.presenter.PaymentPresenterImpl;
import com.finalyearproject.easyride.easyride.utils.ProSwipeButton;
import com.finalyearproject.easyride.easyride.view.BaseActivity;
import com.finalyearproject.easyride.easyride.view.RiderMainActivity;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentActivity extends BaseActivity implements PaymentContract.View {

    @BindView(R.id.toolbar_left_btn)
    ImageView toolbar_left_btn;
    @BindView(R.id.swipe_button)
    ProSwipeButton swipe_button;

    private Boolean isSwiped = false;

    @BindView(R.id.card_type)
    ImageView card_type;
    @BindView(R.id.tv_card_number)
    TextView tv_card_number;
    @BindView(R.id.tv_validity)
    TextView tv_validity;

    @BindView(R.id.txt_total_payable)
    TextView txt_total_payable;
    Handler mHandler = new Handler(Looper.getMainLooper());

    @Inject
    PaymentPresenterImpl mPresenter;

    private Card mCard;
    private String price;
    private String journeyID;
    private String dateTime;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        ButterKnife.bind(this);
        ApplicationComponent applicationComponent = EasyRideApplication.get(this).getAppComponent();
        PaymentComponent paymentComponent = DaggerPaymentComponent.builder()
                .paymentContextModule(new PaymentContextModule(this))
                .paymentPresenterModule(new PaymentPresenterModule(this))
                .applicationComponent(applicationComponent)
                .build();

        paymentComponent.injectPaymentActivity(this);

        price = getIntent().getExtras().equals(null) ? null : getIntent().getExtras().getString("price");
        journeyID = getIntent().getExtras().equals(null) ? null : getIntent().getExtras().getString("journeyID");
        dateTime = getIntent().getExtras().equals(null) ? null : getIntent().getExtras().getString("dateTime");

        if(price==null){
            return;
        }else{
            txt_total_payable.setText("RM "+ price);
        }

        initToolbar();
    }


    @Override
    protected void onStart() {
        super.onStart();

        mPresenter.getCard();
        Executor executor = Executors.newSingleThreadExecutor();

        FragmentActivity activity = this;

        if (executor != null) {
            final BiometricPrompt biometricPrompt = new BiometricPrompt(activity, executor, new BiometricPrompt.AuthenticationCallback() {
                @Override
                public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                    super.onAuthenticationError(errorCode, errString);
                    if (errorCode == BiometricPrompt.ERROR_NEGATIVE_BUTTON) {
                        // user clicked negative button
                        runOnUiThread(new Runnable() {
                            public void run() {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        // task success! show TICK icon in ProSwipeButton
                                        // false if task failed
                                        swipe_button.showResultIcon(false);
                                    }
                                }, 2000);
                            }
                        });

                    } else {
                        //TODO: Called when an unrecoverable error has been encountered and the operation is complete.
                    }
                }

                @Override
                public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
                    super.onAuthenticationSucceeded(result);
                    //TODO: Called when a biometric is recognized.

                    if(journeyID!=null && dateTime != null && price != null)
                        mPresenter.chargePassenger(journeyID, dateTime, price);
                }

                @Override
                public void onAuthenticationFailed() {
                    super.onAuthenticationFailed();
                    //TODO: Called when a biometric is valid but not recognized.

                }

            });

            final BiometricPrompt.PromptInfo promptInfo = new BiometricPrompt.PromptInfo.Builder()
                    .setTitle("Approve the payment with fingerprint scan or close the request.")
                    .setNegativeButtonText("Close")
                    .build();

            swipe_button.setOnSwipeListener(new ProSwipeButton.OnSwipeListener() {
                @Override
                public void onSwipeConfirm() {
                    // user has swiped the btn. Perform your async operation now
                    biometricPrompt.authenticate(promptInfo);

                }

            });
        }
    }

  /*  public void onPaymentDone(PaymentDetails paymentDetails){
        mPaymentDetails = paymentDetails;
    }*/

    public void presentActivity(View view) {

        Toast.makeText(this, "Payment Successful", Toast.LENGTH_SHORT).show();
        finish();
        Intent intent = new Intent(this, RiderMainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }


    private void initToolbar() {
        toolbar_left_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void onShowCard(Card card) {
        mCard = card;

        if (card.getBrand().equals("Visa")) {
            card_type.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_payment_white_visa));
        } else {
            card_type.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_payment_master_card));
        }

        String lastTwoDigits = "";
        tv_card_number.setText("\u25CF\u25CF\u25CF\u25CF \u25CF\u25CF\u25CF\u25CF \u25CF\u25CF\u25CF\u25CF " + card.getLast4());
        lastTwoDigits = String.valueOf(card.getExp_year()).substring(String.valueOf(card.getExp_year()).length() - 2);
        tv_validity.setText(String.valueOf(card.getExp_month()) + "/" + lastTwoDigits);
    }

    @Override
    public void onCardDeleted() {

    }

    @Override
    public void onNext(String message) {
        runOnUiThread(new Runnable() {
            public void run() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // task success! show TICK icon in ProSwipeButton
                        // false if task failed
                        swipe_button.showResultIcon(true, true);
                        isSwiped = true;
                    }
                }, 2000);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // task success! show TICK icon in ProSwipeButton
                        presentActivity(swipe_button);
                    }

                }, 2500);
            }
        });
    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void showComplete() {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }


}
