package com.finalyearproject.easyride.easyride.Earnings.model;

import com.google.gson.annotations.SerializedName;

public class EarningInfo {
    @SerializedName("driverResponse")
    private String driverResponse;
    @SerializedName("dateTime")
    private String dateTime;
    @SerializedName("amount")
    private String amount;

    public String getDriverResponse() {
        return driverResponse;
    }

    public void setDriverResponse(String driverResponse) {
        this.driverResponse = driverResponse;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

}

