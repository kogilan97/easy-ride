package com.finalyearproject.easyride.easyride.Driver.utils;

import com.google.android.gms.maps.model.PolylineOptions;

public interface DirectionPointListener {
    public void onPath(PolylineOptions polyLine);
}
