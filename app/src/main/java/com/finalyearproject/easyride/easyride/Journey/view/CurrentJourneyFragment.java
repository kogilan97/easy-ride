package com.finalyearproject.easyride.easyride.Journey.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.finalyearproject.easyride.R;
import com.finalyearproject.easyride.easyride.Journey.model.JourneyInfo;
import com.finalyearproject.easyride.easyride.Journey.presenter.JourneyContract;
import com.finalyearproject.easyride.easyride.Journey.presenter.JourneyPresenterImpl;
import com.finalyearproject.easyride.easyride.view.DriverMainActivity;
import com.finalyearproject.easyride.easyride.view.MainFragment;
import com.finalyearproject.easyride.easyride.view.RiderMainActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class CurrentJourneyFragment extends MainFragment implements JourneyFragment.OnTabChangeListener, JourneyAdapter.JourneyItemClickListener, JourneyContract.View, DriverJourneyAdapter.JourneyItemClickListener {

    public static CurrentJourneyFragment getInstance() {
        return new CurrentJourneyFragment();
    }

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    private JourneyAdapter mAdapter;
    private DriverJourneyAdapter mDriverAdapter;
    private Unbinder mUnBinder;

    JourneyPresenterImpl mPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        JourneyFragment.setOnTabChangeListener(this);
        mPresenter = new JourneyPresenterImpl(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_current_journey,container,false);
        mUnBinder = ButterKnife.bind(this,view);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if(getActivity() instanceof RiderMainActivity)
            mPresenter.getJourneyList("Current");
        else if (getActivity() instanceof DriverMainActivity)
            mPresenter.getDriverJourneyList("Current");
    }

    @Override
    public void onTabChange() {

    }

    @Override
    public void onItemClick(String journeyID) {
        if(getActivity() instanceof RiderMainActivity) {
            Intent intent = new Intent(CurrentJourneyFragment.this.getActivity(),JourneyDetailActivity.class);
            intent.putExtra("journeyID",journeyID);
            startActivity(intent);
        }else if (getActivity() instanceof DriverMainActivity){
            Intent intent = new Intent(CurrentJourneyFragment.this.getActivity(),DriverJourneyDetails.class);
            intent.putExtra("journeyID",journeyID);
            startActivity(intent);
        }
    }

    @Override
    public void onNext(String message) {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void onShowJourneyList(List<JourneyInfo> journeyInfoList) {
        if(getActivity() instanceof RiderMainActivity) {
            mAdapter = new JourneyAdapter(this.getContext(), journeyInfoList);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.setAdapter(mAdapter);
            mAdapter.setOnItemClickListener(this);
        }else if (getActivity() instanceof DriverMainActivity){
            mDriverAdapter = new DriverJourneyAdapter(this.getContext(), journeyInfoList);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.setAdapter(mDriverAdapter);
            mDriverAdapter.setOnItemClickListener(this);
        }
    }


    @Override
    public void showComplete() {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }
}
