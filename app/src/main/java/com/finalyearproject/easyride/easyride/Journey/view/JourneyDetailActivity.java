package com.finalyearproject.easyride.easyride.Journey.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.bumptech.glide.Glide;
import com.finalyearproject.easyride.R;
import com.finalyearproject.easyride.database.EasyRideStorage;
import com.finalyearproject.easyride.easyride.Journey.model.JourneyInfo;
import com.finalyearproject.easyride.easyride.Journey.presenter.JourneyPresenterImpl;
import com.finalyearproject.easyride.easyride.Payment.view.ActiveCardActivity;
import com.finalyearproject.easyride.easyride.Payment.view.PaymentActivity;
import com.finalyearproject.easyride.easyride.view.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class JourneyDetailActivity extends BaseActivity {

    @BindView(R.id.btn_pay_now)
    TextView btn_pay_now;
    @BindView(R.id.btn_phone)
    ImageView btn_phone;
    @BindView(R.id.txt_driver_name)
    TextView txt_driver_name;
    @BindView(R.id.txt_car_details)
    TextView txt_car_details;
    @BindView(R.id.btn_cancel_journey)
    Button btn_cancel_journey;
    @BindView(R.id.static_map)
    ImageView static_map;
    @BindView(R.id.status)
    TextView status;
    @BindView(R.id.txt_price)
    TextView txt_price;
    @BindView(R.id.img_car)
    ImageView img_car;
    @BindView(R.id.img_user)
    ImageView img_user;



    JourneyPresenterImpl mPresenter;
    String journeyID;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journey_details);
        ButterKnife.bind(this);
        mPresenter = new JourneyPresenterImpl(this);
        journeyID = getIntent().getExtras().equals(null) ? null : getIntent().getExtras().getString("journeyID");


    }

    @Override
    protected void onStart() {
        super.onStart();
        if (journeyID != null)
            mPresenter.getJourneyDetails(journeyID);
    }

    public void onGetDetails(JourneyInfo journeyInfo) {
        txt_driver_name.setText(journeyInfo.getDriverInfo().getUsername());
        txt_car_details.setText(journeyInfo.getCarInfo().getCarnumber() + " (" + journeyInfo.getCarInfo().getCarmodel() + ")");
        txt_price.setText("RM "+journeyInfo.getPrice());
        Glide
                .with(this)
                .load(Uri.parse(journeyInfo.getCarInfo().getImage_file()))
                .centerCrop()
                .thumbnail(0.1f)
                .into(img_car);

        Glide
                .with(this)
                .load(Uri.parse("https://car-images-easyride.s3.amazonaws.com/uploadImage/vishanth.jpg"))
                .centerCrop()
                .thumbnail(0.1f)
                .into(img_user);

        btn_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(JourneyDetailActivity.this);
                builder.setTitle("Select Option");
                String[] items = new String[]{"Call", "Whatsapp"};
                builder.setItems(items, (dialog, which) -> {
                    if (which == 1) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=+6" + journeyInfo.getDriverInfo().getPhone()));
                        startActivity(intent);
                    }
                    if (which == 0) {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:" + journeyInfo.getDriverInfo().getPhone()));
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });
                builder.show();
            }
        });

        btn_cancel_journey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.cancelJourney(journeyInfo.get_id());
            }
        });

        btn_pay_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(EasyRideStorage.getInstance().getIsCreditCardAdded()){
                    Intent intent = new Intent(JourneyDetailActivity.this, PaymentActivity.class);
                    intent.putExtra("price", journeyInfo.getPrice());
                    intent.putExtra("journeyID", journeyInfo.get_id());
                    intent.putExtra("dateTime", journeyInfo.getDateTime());

                    startActivity(intent);
                }else{
                    startActivity(new Intent(JourneyDetailActivity.this, ActiveCardActivity.class));
                }
            }
        });

        Glide
                .with(this)
                .load(Uri.parse("https://maps.googleapis.com/maps/api/staticmap?autoscale=2&size=1920x1080&maptype=roadmap&key=" + getString(R.string.google_places_api_key) + "&format=png&visual_refresh=true&markers=size:mid%7Ccolor:0xff0000%7Clabel:A%7C" + journeyInfo.getStartPointLat() + "," + journeyInfo.getStartPointLong() + "&markers=size:mid%7Ccolor:0xff0000%7Clabel:B%7C" + journeyInfo.getEndPointLat() + "," + journeyInfo.getEndPointLong()))
                .fitCenter()
                .thumbnail(0.1f)
                .into(static_map);

        switch (journeyInfo.getDriverResponse()) {
            case "Pending":
                btn_cancel_journey.setVisibility(View.VISIBLE);
                btn_pay_now.setVisibility(View.GONE);
                status.setText("Pending");
                status.setBackground(getResources().getDrawable(R.drawable.bg_pending));
                break;
            case "Accepted":
                btn_cancel_journey.setVisibility(View.VISIBLE);
                btn_pay_now.setVisibility(View.GONE);
                status.setText("Accepted");
                status.setBackground(getResources().getDrawable(R.drawable.bg_accepted));
                break;

            case "Started":
                btn_cancel_journey.setVisibility(View.GONE);
                btn_pay_now.setVisibility(View.VISIBLE);
                status.setText("Started");
                status.setBackground(getResources().getDrawable(R.drawable.bg_started));
                break;

            case "Declined":
                btn_cancel_journey.setVisibility(View.GONE);
                btn_pay_now.setVisibility(View.GONE);
                status.setText("Declined");
                status.setBackground(getResources().getDrawable(R.drawable.bg_declined));
                break;
            case "Completed":
                btn_cancel_journey.setVisibility(View.GONE);
                btn_pay_now.setVisibility(View.GONE);
                status.setText("Completed");
                status.setBackground(getResources().getDrawable(R.drawable.bg_completed));
                break;
        }
    }

    public void onNext(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        finish();
    }
}
