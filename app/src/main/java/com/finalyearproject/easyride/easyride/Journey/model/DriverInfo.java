package com.finalyearproject.easyride.easyride.Journey.model;

public class DriverInfo {
    public String _id ;
    public String username;
    public String email;
    public String phone;
    public boolean emailverification;
    public boolean phoneverification;
    public String approle;
    public String createdAt;
    public String updatedAt;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isEmailverification() {
        return emailverification;
    }

    public void setEmailverification(boolean emailverification) {
        this.emailverification = emailverification;
    }

    public boolean isPhoneverification() {
        return phoneverification;
    }

    public void setPhoneverification(boolean phoneverification) {
        this.phoneverification = phoneverification;
    }

    public String getApprole() {
        return approle;
    }

    public void setApprole(String approle) {
        this.approle = approle;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
