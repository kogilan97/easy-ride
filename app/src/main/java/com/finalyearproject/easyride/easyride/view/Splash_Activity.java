package com.finalyearproject.easyride.easyride.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.os.Handler;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.finalyearproject.easyride.EasyRideConstants;
import com.finalyearproject.easyride.R;
import com.finalyearproject.easyride.database.EasyRideStorage;
import com.finalyearproject.easyride.easyride.LoginRegister.view.LoginActivity;

public class Splash_Activity extends BaseActivity {
    private ImageView logo;
    private TextView logotext;
    private static int splashtimeout = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        logo = (ImageView) findViewById(R.id.easyapplogo);
        logotext= (TextView) findViewById(R.id.easyapplogotext);

        Animation myanim = AnimationUtils.loadAnimation(this,R.anim.anim_splashscreen);
        logo.startAnimation(myanim);
        logotext.startAnimation(myanim);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        new Handler().postDelayed(new Runnable() {
           @Override
            public void run() {
             //  Log.d("tag2",EasyRideStorage.getInstance().getAuthToken());
               if(EasyRideStorage.getInstance().getAuthToken() != null && EasyRideStorage.getInstance().getPrefLoggedIn()){
                   if(EasyRideStorage.getInstance().getCurrentRole() == EasyRideConstants.ROLE_USER){
                       Intent i = new Intent(Splash_Activity.this, RiderMainActivity.class);
                       startActivity(i);
                   }else if(EasyRideStorage.getInstance().getCurrentRole() == EasyRideConstants.ROLE_DRIVER){
                       Intent i = new Intent(Splash_Activity.this, DriverMainActivity.class);
                       startActivity(i);
                   }
                   finish();
               }else {

                   Intent i = new Intent(Splash_Activity.this, LoginActivity.class);
                   startActivity(i);
                   finish();
               }
         }
        },splashtimeout);


        }
    }

