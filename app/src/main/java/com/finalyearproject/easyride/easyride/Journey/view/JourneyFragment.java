package com.finalyearproject.easyride.easyride.Journey.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.finalyearproject.easyride.R;
import com.finalyearproject.easyride.easyride.view.MainFragment;
import com.google.android.material.tabs.TabLayout;

public class JourneyFragment extends MainFragment {

    private static OnTabChangeListener mOnTabChangeListener;
    private FragmentPagerAdapter mPagerAdapter;
    private ViewPager mViewPager;

    public static JourneyFragment getInstance() {
        return new JourneyFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_journey, container, false);

        mViewPager = view.findViewById(R.id.view_pager);
        TabLayout tabLayout = view.findViewById(R.id.tab_layout);

        mPagerAdapter = new JourneyTabAdapter(getChildFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);
        tabLayout.setTabsFromPagerAdapter(mPagerAdapter);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
                mOnTabChangeListener.onTabChange();
                mPagerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                // do nothing
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
                mOnTabChangeListener.onTabChange();
                mPagerAdapter.notifyDataSetChanged();
            }
        });

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        mPagerAdapter.notifyDataSetChanged();

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public static void setOnTabChangeListener(OnTabChangeListener callback) {
        mOnTabChangeListener = callback;
    }

    public interface OnTabChangeListener {
        void onTabChange();
    }
}
