package com.finalyearproject.easyride.easyride.EditProfile.presenter;

import android.widget.Toast;

import com.finalyearproject.easyride.data.UserModalClass;
import com.finalyearproject.easyride.database.EasyRideStorage;
import com.finalyearproject.easyride.di.module.APIInterface;
import com.finalyearproject.easyride.di.module.RetrofitModule;
import com.finalyearproject.easyride.easyride.EditProfile.view.RiderEditProfileActivity;
import com.finalyearproject.easyride.easyride.EditProfile.view.RiderProfileFragment;
import com.finalyearproject.easyride.easyride.EditProfile.view.VerifyPhoneActivity;
import com.microblink.entities.recognizers.RecognizerBundle;
import com.microblink.entities.recognizers.blinkid.generic.BlinkIdCombinedRecognizer;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.finalyearproject.easyride.EasyRideApplication.getContext;

public class EditProfilePresenterImpl implements EditProfileInterface.Presenter {

    private RiderEditProfileActivity riderEditProfileActivity;
    private RiderProfileFragment riderProfileFragment;
    private VerifyPhoneActivity verifyPhoneActivity;
    private APIInterface mAPIInterface;
    private BlinkIdCombinedRecognizer mRecognizer;
    private RecognizerBundle mRecognizerBundle;


    public EditProfilePresenterImpl(RiderEditProfileActivity view) {
        this.riderEditProfileActivity = view;
        mAPIInterface = RetrofitModule.getApiInterface();
    }

    public EditProfilePresenterImpl(VerifyPhoneActivity view) {
        this.verifyPhoneActivity = view;
        mAPIInterface = RetrofitModule.getApiInterface();
    }

    public EditProfilePresenterImpl(RiderProfileFragment view) {
        this.riderProfileFragment = view;
        mAPIInterface = RetrofitModule.getApiInterface();
    }

    @Override
    public void onFetchDetails() {

        JSONObject object = new JSONObject();
        try {
            object.put("userId", EasyRideStorage.getInstance().getPrefUserId());

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("raw"), object.toString());
        mAPIInterface.getProfileDetails(body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        riderEditProfileActivity.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(UserModalClass userModalClass) {
                        riderEditProfileActivity.onNext(userModalClass.getMessage(),userModalClass.getStatus(),userModalClass.getUserInfo());

                    }
                });
    }

    public void onEmailVerify()
    {
        JSONObject object = new JSONObject();
        try {
            object.put("userId", EasyRideStorage.getInstance().getPrefEmailAddr());

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("raw"), object.toString());
        mAPIInterface.verifyEmail(body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        riderEditProfileActivity.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(UserModalClass userModalClass) {
                        riderEditProfileActivity.onVerifyEmailNext(userModalClass.getMessage());
                    }
                });
    }

    public void OnICVerify()
    {
        mAPIInterface.verifyIC(EasyRideStorage.getInstance().getAuthToken(),EasyRideStorage.getInstance().getPrefUserId()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        riderEditProfileActivity.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(UserModalClass userModalClass) {
                        riderEditProfileActivity.onVerifyICNext(userModalClass.getMessage());
                    }
                });
    }


    public void onLicenseVerify()
    {
        mAPIInterface.verifyLicense(EasyRideStorage.getInstance().getAuthToken(),EasyRideStorage.getInstance().getPrefUserId()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        riderEditProfileActivity.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(UserModalClass userModalClass) {
                        riderEditProfileActivity.onVerifyLicenseNext(userModalClass.getMessage());
                    }
                });
    }

    public void requestPhoneVerification(String number){
        mAPIInterface.makePhoneRequest(EasyRideStorage.getInstance().getAuthToken(),number).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(UserModalClass userModalClass) {
                        riderEditProfileActivity.onRequested(userModalClass.getResult().getRequest_id());
                    }
                });
    }

    public void placeVerification(String id, String code){

        JSONObject object = new JSONObject();
        try {
            object.put("reqId", id);
            object.put("userId", EasyRideStorage.getInstance().getPrefUserId());
            object.put("reqCode", code);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("raw"),object.toString());


        mAPIInterface.validatePhoneRequest(EasyRideStorage.getInstance().getAuthToken(),body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(UserModalClass userModalClass) {
                        if(userModalClass.getStatus()){
                            verifyPhoneActivity.onSuccess(userModalClass.getMessage());
                        }
                        else
                        {
                            Toast.makeText(getContext(), "Incorrect Pin Number",Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }

    public void checkVerification(){
        JSONObject object = new JSONObject();
        try {
            object.put("userId", EasyRideStorage.getInstance().getPrefUserId());

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("raw"), object.toString());
        mAPIInterface.getProfileDetails(body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        riderProfileFragment.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(UserModalClass userModalClass) {
                        riderProfileFragment.onNext(userModalClass.getMessage(),userModalClass.getStatus(),userModalClass.getUserInfo());
                    }
                });
    }

}
