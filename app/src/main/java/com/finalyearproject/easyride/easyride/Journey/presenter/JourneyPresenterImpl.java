package com.finalyearproject.easyride.easyride.Journey.presenter;

import android.content.Context;
import android.util.Log;
import android.view.View;

import androidx.fragment.app.Fragment;

import com.finalyearproject.easyride.data.UserModalClass;
import com.finalyearproject.easyride.database.EasyRideStorage;
import com.finalyearproject.easyride.di.module.APIInterface;
import com.finalyearproject.easyride.di.module.RetrofitModule;
import com.finalyearproject.easyride.easyride.Journey.view.CurrentJourneyFragment;
import com.finalyearproject.easyride.easyride.Journey.view.DriverJourneyDetails;
import com.finalyearproject.easyride.easyride.Journey.view.JourneyDetailActivity;
import com.finalyearproject.easyride.easyride.Journey.view.PastJourneyFragment;
import com.finalyearproject.easyride.easyride.Journey.view.UpcomingJourneyFragment;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class JourneyPresenterImpl implements JourneyContract.Presenter{

    private APIInterface mApiInterface;
    private CurrentJourneyFragment currentJourneyFragment;
    private UpcomingJourneyFragment upcomingJourneyFragment;
    private PastJourneyFragment pastJourneyFragment;
    private JourneyDetailActivity journeyDetailActivity;
    private DriverJourneyDetails driverJourneyDetails;

    public JourneyPresenterImpl(CurrentJourneyFragment currentJourneyFragment) {
        this.currentJourneyFragment = currentJourneyFragment;
        mApiInterface = RetrofitModule.getApiInterface();
    }

    public JourneyPresenterImpl(PastJourneyFragment pastJourneyFragment) {
        this.pastJourneyFragment = pastJourneyFragment;
        mApiInterface = RetrofitModule.getApiInterface();
    }

    public JourneyPresenterImpl(UpcomingJourneyFragment upcomingJourneyFragment) {
        this.upcomingJourneyFragment = upcomingJourneyFragment;
        mApiInterface = RetrofitModule.getApiInterface();
    }

    public JourneyPresenterImpl(JourneyDetailActivity journeyDetailActivity) {
        this.journeyDetailActivity = journeyDetailActivity;
        mApiInterface = RetrofitModule.getApiInterface();
    }

    public JourneyPresenterImpl(DriverJourneyDetails driverJourneyDetails){
        this.driverJourneyDetails = driverJourneyDetails;
        mApiInterface = RetrofitModule.getApiInterface();
    }


    @Override
    public void getJourneyList(String status) {

        mApiInterface.getUserJourney(EasyRideStorage.getInstance().getAuthToken(), EasyRideStorage.getInstance().getPrefUserId(),status).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {

                    @Override
                    public void onCompleted() {
                        if(currentJourneyFragment !=null)
                            currentJourneyFragment.showComplete();
                        else if(pastJourneyFragment !=null)
                            pastJourneyFragment.showComplete();
                        else if(upcomingJourneyFragment != null)
                            upcomingJourneyFragment.showComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(currentJourneyFragment !=null)
                            currentJourneyFragment.showError(e.toString());
                        else if(pastJourneyFragment !=null)
                            pastJourneyFragment.showError(e.toString());
                        else if(upcomingJourneyFragment != null)
                            upcomingJourneyFragment.showError(e.toString());

                    }

                    @Override
                    public void onNext(UserModalClass status) {
                        if(currentJourneyFragment !=null) {
                            currentJourneyFragment.hideProgress();
                            currentJourneyFragment.onShowJourneyList(status.getJourneyInfoList());
                        }
                        else if(pastJourneyFragment !=null) {
                            pastJourneyFragment.hideProgress();
                            pastJourneyFragment.onShowJourneyList(status.getJourneyInfoList());
                        }
                        else if(upcomingJourneyFragment != null) {
                            upcomingJourneyFragment.hideProgress();
                            upcomingJourneyFragment.onShowJourneyList(status.getJourneyInfoList());
                        }

                    }
                });
    }

    public void getDriverJourneyList(String status) {

        mApiInterface.getDriverJourney(EasyRideStorage.getInstance().getAuthToken(), EasyRideStorage.getInstance().getPrefUserId(),status).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {

                    @Override
                    public void onCompleted() {
                        if(currentJourneyFragment !=null)
                            currentJourneyFragment.showComplete();
                        else if(pastJourneyFragment !=null)
                            pastJourneyFragment.showComplete();
                        else if(upcomingJourneyFragment != null)
                            upcomingJourneyFragment.showComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(currentJourneyFragment !=null)
                            currentJourneyFragment.showError(e.toString());
                        else if(pastJourneyFragment !=null)
                            pastJourneyFragment.showError(e.toString());
                        else if(upcomingJourneyFragment != null)
                            upcomingJourneyFragment.showError(e.toString());

                    }

                    @Override
                    public void onNext(UserModalClass status) {
                        if(currentJourneyFragment !=null) {
                            currentJourneyFragment.hideProgress();
                            currentJourneyFragment.onShowJourneyList(status.getJourneyInfoList());
                        }
                        else if(pastJourneyFragment !=null) {
                            pastJourneyFragment.hideProgress();
                            pastJourneyFragment.onShowJourneyList(status.getJourneyInfoList());
                        }
                        else if(upcomingJourneyFragment != null) {
                            upcomingJourneyFragment.hideProgress();
                            upcomingJourneyFragment.onShowJourneyList(status.getJourneyInfoList());
                        }

                    }
                });
    }



    @Override
    public void getJourneyDetails(String journeyID) {
        mApiInterface.getJourneyDetails(EasyRideStorage.getInstance().getAuthToken(), journeyID).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(UserModalClass status) {
                       journeyDetailActivity.onGetDetails(status.getJourneyInfo());
                    }
                });
    }

    public void getDriverJourneyDetails(String journeyID) {
        mApiInterface.getDriverJourneyDetails(EasyRideStorage.getInstance().getAuthToken(), journeyID).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(UserModalClass status) {
                        driverJourneyDetails.onGetDetails(status.getJourneyInfo());
                    }
                });
    }

    public void acceptJourney(String journeyID) {
        mApiInterface.acceptJourney(EasyRideStorage.getInstance().getAuthToken(), journeyID).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(UserModalClass status) {
                        driverJourneyDetails.onNext(status.getMessage());
                    }
                });
    }

    public void startJourney(String journeyID) {
        mApiInterface.startJourney(EasyRideStorage.getInstance().getAuthToken(), journeyID).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(UserModalClass status) {
                        driverJourneyDetails.onNext(status.getMessage());
                    }
                });
    }

    public void rejectJourney(String journeyID) {
        mApiInterface.rejectJourney(EasyRideStorage.getInstance().getAuthToken(), journeyID).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(UserModalClass status) {
                        driverJourneyDetails.onNext(status.getMessage());
                    }
                });
    }

    public void completeJourney(String journeyID) {
        mApiInterface.completeJourney(EasyRideStorage.getInstance().getAuthToken(), journeyID).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(UserModalClass status) {
                        driverJourneyDetails.onNext(status.getMessage());
                    }
                });
    }

    public void cancelJourney(String journeyID) {
        mApiInterface.cancelJourney(EasyRideStorage.getInstance().getAuthToken(), journeyID).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(UserModalClass status) {
                        journeyDetailActivity.onNext(status.getMessage());
                    }
                });
    }
}
