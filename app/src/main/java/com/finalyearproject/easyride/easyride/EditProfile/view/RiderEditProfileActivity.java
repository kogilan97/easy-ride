package com.finalyearproject.easyride.easyride.EditProfile.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.finalyearproject.easyride.R;
import com.finalyearproject.easyride.easyride.EditProfile.presenter.EditProfileInterface;
import com.finalyearproject.easyride.easyride.EditProfile.presenter.EditProfilePresenterImpl;

import com.finalyearproject.easyride.easyride.LoginRegister.model.UserInfo;
import com.finalyearproject.easyride.easyride.view.BaseActivity;

import com.microblink.entities.recognizers.Recognizer;
import com.microblink.entities.recognizers.RecognizerBundle;
import com.microblink.entities.recognizers.blinkid.generic.BlinkIdCombinedRecognizer;
import com.microblink.uisettings.ActivityRunner;
import com.microblink.uisettings.BlinkIdUISettings;

public class RiderEditProfileActivity extends BaseActivity implements EditProfileInterface.View {

    private ImageView imgbackbutton;
    private TextView txtusername;
    private TextView txtphonenumber;
    private TextView txtphone;
    private TextView txtemailaddress;
    private TextView txtpassword;
    private Button btnverifyemail;
    private Button btnverifyphone;
    private Button btnverifyic;
    private Button btnverifylicense;
    private BlinkIdCombinedRecognizer mRecognizer;
    private RecognizerBundle mRecognizerBundle;
    private int MY_REQUEST_CODE;

    EditProfilePresenterImpl mPresenter;
    int licenseFlag =0,icFlag=0;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rider_editprofile);

        // create BlinkIdCombinedRecognizer
        mRecognizer = new BlinkIdCombinedRecognizer();

        // bundle recognizers into RecognizerBundle
        mRecognizerBundle = new RecognizerBundle(mRecognizer);

       //
        imgbackbutton = findViewById(R.id.ImageEditProfileBack);
        txtusername = findViewById(R.id.txtusername);
        txtphonenumber = findViewById(R.id.txtphone);
        txtemailaddress = findViewById(R.id.txtemail);
        btnverifyemail = findViewById(R.id.btnemailverify);
        btnverifyphone = findViewById(R.id.btnphoneverify);
        btnverifyic = findViewById(R.id.btnicverify);
        btnverifylicense=findViewById(R.id.btnlicenseverify);

        mPresenter = new EditProfilePresenterImpl(this);


        btnverifyphone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.requestPhoneVerification(txtphonenumber.getText().toString());
            }
        });

        imgbackbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnverifyemail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VerifyEmail();
            }
        });

        btnverifyic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VerifyIC();
            }
        });

        btnverifylicense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VerifyLicense();
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.onFetchDetails();
    }

    @Override
    public void onNext(String msg, Boolean status, UserInfo userInfoClass) {
        txtusername.setText(userInfoClass.getUsername());
        txtemailaddress.setText(userInfoClass.getEmail());
        txtphonenumber.setText(userInfoClass.getPhonenumber());
        if (!userInfoClass.getEmailverification()) {
            btnverifyemail.setEnabled(true);
            btnverifyemail.setText("Verify");
        } else {
            btnverifyemail.setEnabled(false);
            btnverifyemail.setText("Verified");
        }

        if (!userInfoClass.getPhoneverification()) {
            btnverifyphone.setEnabled(true);
            btnverifyphone.setText("Verify");
        } else {
            btnverifyphone.setEnabled(false);
            btnverifyphone.setText("Verified");
        }

        if (!userInfoClass.getICverification()) {
            btnverifyic.setEnabled(true);
            btnverifyic.setText("Verify");
        } else {
            btnverifyic.setEnabled(false);
            btnverifyic.setText("Verified");
        }

        if (!userInfoClass.getLicenseverification()) {
            btnverifylicense.setEnabled(true);
            btnverifylicense.setText("Verify");
        } else {
            btnverifylicense.setEnabled(false);
            btnverifylicense.setText("Verified");
        }

    }

    @Override
    public void showError(String msg) {
        Log.d("TAG", msg);

    }

    public void onRequested(String id){
        Intent intent = new Intent(RiderEditProfileActivity.this, VerifyPhoneActivity.class);
        intent.putExtra("id",id);
        startActivity(intent);
    }

    public  void VerifyEmail()
    {
        mPresenter.onEmailVerify();
    }
    public void onVerifyEmailNext(String msg)
    {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        finish();
    }

    public  void VerifyIC()
    {
        icFlag=1;
        startScanning();
    }


    public void VerifyLicense() {

        licenseFlag = 1;
        startScanning();

    }

    // method within MyActivity from previous step
    public void startScanning() {

        // Settings for BlinkIdActivity
        BlinkIdUISettings settings = new BlinkIdUISettings(mRecognizerBundle);

        // tweak settings as you wish

        // Start activity
        ActivityRunner.startActivityForResult(this, MY_REQUEST_CODE, settings);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                // load the data into all recognizers bundled within your RecognizerBundle
                mRecognizerBundle.loadFromIntent(data);

                // now every recognizer object that was bundled within RecognizerBundle
                // has been updated with results obtained during scanning session

                // you can get the result by invoking getResult on recognizer
                BlinkIdCombinedRecognizer.Result result = mRecognizer.getResult();
                if (result.getResultState() == Recognizer.Result.State.Valid) {

                    if(icFlag==1)
                    {
                        mPresenter.OnICVerify();
                        icFlag=0;
                    }
                    if(licenseFlag==1)
                    {
                        mPresenter.onLicenseVerify();
                        licenseFlag=0;
                    }

                }
            }
        }
    }

    public void onVerifyICNext(String msg)
    {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public void onVerifyLicenseNext(String msg)
    {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

}
