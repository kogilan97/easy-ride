package com.finalyearproject.easyride.easyride.CarManagement.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.finalyearproject.easyride.R;
import com.finalyearproject.easyride.easyride.CarManagement.model.CarAdapterClass;
import com.finalyearproject.easyride.easyride.CarManagement.model.CarDetailsClass;
import com.finalyearproject.easyride.easyride.CarManagement.presenter.CarsInterface;
import com.finalyearproject.easyride.easyride.CarManagement.presenter.CarsPresenterImpl;

import java.util.List;

public class DriverCarsFragment extends Fragment implements CarsInterface.View,CarAdapterClass.itemClickListener {
    private ImageView addbutton;
    CarsPresenterImpl mPresenter;
    RecyclerView recyclerview;
    CarAdapterClass caradapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        View view = inflater.inflate(R.layout.fragment_driver_cars, container, false);
        recyclerview = (RecyclerView) view.findViewById(R.id.recyclerView);


        addbutton = view.findViewById(R.id.btn_add_new_car);
        addbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DriverAddCarActivity.class);
                startActivity(intent);
            }
        });

        mPresenter = new CarsPresenterImpl(this);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.onFetchDetails();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Driver Cars");
    }

    @Override
    public void onNext(String msg, List<CarDetailsClass> CarDetailsClass) {

        recyclerview.setHasFixedSize(true);
        recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        caradapter = new CarAdapterClass(getActivity(), CarDetailsClass);
        recyclerview.setAdapter(caradapter);
        caradapter.setOnItemClickListener(this);

    }

    @Override
    public void showError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onItemClicked(String id) {
        mPresenter.onDeleteCar(id);
    }

    public void afterDelete(String message,Boolean Status)
    {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        onStart();
    }
}
