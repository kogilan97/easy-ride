package com.finalyearproject.easyride.easyride.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.finalyearproject.easyride.R;

public class PopupToolbar extends Toolbar {

    TextView title, leftAction, rightAction;
    ImageView backButton;

    private void addView(Context context) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View child = inflater.inflate(R.layout.layout_popup_actionbar, null);
        addView(child);

        backButton = child.findViewById(R.id.backButton);
        title = child.findViewById(R.id.topBarTitle);
        leftAction = child.findViewById(R.id.leftAction);
        rightAction = child.findViewById(R.id.rightAction);
    }

    private void updateAttrs(TypedArray typedArray){
        String titleText = typedArray.getString(R.styleable.PopupToolBar_titleText);
        String leftActionText = typedArray.getString(R.styleable.PopupToolBar_leftActionText);
        String rightActionText = typedArray.getString(R.styleable.PopupToolBar_rightActionText);

        if(titleText!=null
                && !titleText.isEmpty()){
            title.setText(titleText);
        }

        if(leftActionText!=null
                && !leftActionText.isEmpty()){
            leftAction.setText(leftActionText);
        }

        if(rightActionText!=null
                && !rightActionText.isEmpty()){
            rightAction.setText(rightActionText);
        }

        typedArray.recycle();
    }

    public PopupToolbar(Context context) {
        super(context);
        addView(context);
    }

    public PopupToolbar(Context context,  AttributeSet attrs) {
        super(context, attrs);
        addView(context);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.PopupToolBar);
        updateAttrs(typedArray);
    }

    public PopupToolbar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        addView(context);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.PopupToolBar, defStyleAttr, 0);
        updateAttrs(typedArray);
    }

    public TextView getLeftAction() {
        return leftAction;
    }

    public TextView getRightAction() {
        return rightAction;
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }

    public void attachViewPager(ViewPager viewPager) {

        if(viewPager==null){
            return;
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    backButton.setVisibility(GONE);
                    leftAction.setVisibility(View.VISIBLE);
                    rightAction.setVisibility(View.VISIBLE);
                }

                if (position > 0) {
                    backButton.setVisibility(View.VISIBLE);
                    leftAction.setVisibility(View.GONE);
                    rightAction.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        backButton.setOnClickListener(v -> {
            if (viewPager.getCurrentItem() > 0) {
                viewPager.setCurrentItem(0, true);
            }
        });
    }
}
