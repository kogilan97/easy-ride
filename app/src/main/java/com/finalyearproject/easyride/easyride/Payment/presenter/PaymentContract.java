package com.finalyearproject.easyride.easyride.Payment.presenter;

import android.content.Context;

import com.finalyearproject.easyride.easyride.Payment.model.Card;

public interface PaymentContract {

    interface View{
        void onShowCard(Card card);

        void onCardDeleted();

        void onNext(String message);

        void showError(String message);

        void showComplete();

        void showProgress();

        void hideProgress();
    }

    interface Presenter{
        void getCard();

        void createPayment();

        void chargePassenger(String journeyId, String dateTime, String amount);

        void deleteCard();

        void uploadCard(String number, String month, String year, String cvv, Context context);
    }
}
