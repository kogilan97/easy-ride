package com.finalyearproject.easyride.easyride.Journey.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.finalyearproject.easyride.R;
import com.finalyearproject.easyride.easyride.Journey.model.JourneyInfo;
import com.finalyearproject.easyride.easyride.utils.Utils;
import com.google.android.gms.maps.model.Circle;

import java.util.Date;
import java.util.List;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class JourneyAdapter extends RecyclerView.Adapter<JourneyAdapter.HistoryViewHolder> {

    private LayoutInflater inflater;
    private JourneyItemClickListener mCallback;
    private List<JourneyInfo> mJourneyInfo;
    private Context context;


    JourneyAdapter(Context context,List<JourneyInfo> journeyInfoList){
        inflater = LayoutInflater.from(context);
        this.mJourneyInfo = journeyInfoList;
        this.context = context;
    }

    @NonNull
    @Override
    public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_journey_items,parent,false);
        return new HistoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryViewHolder holder, int position) {

        final JourneyInfo journeyInfo=mJourneyInfo.get(position);

        holder.mStatus.setText(journeyInfo.getDriverResponse());

        switch (journeyInfo.getDriverResponse()) {
            case "Pending":
                holder.mStatus.setBackground(holder.pending);
                break;
            case "Accepted":
                holder.mStatus.setBackground(holder.accepted);
                break;

            case "Started":
                holder.mStatus.setBackground(holder.started);
                break;

            case "Declined":
                holder.mStatus.setBackground(holder.declined);
                break;
            case "Completed":
                holder.mStatus.setBackground(holder.completed);
                break;
            case "Paid":
                holder.mStatus.setBackground(holder.started);
                break;
            case "Cancelled":
                holder.mStatus.setBackground(holder.declined);
                break;
        }

        holder.car_number_tv.setText(journeyInfo.getCarNumber());
        holder.car_type_tv.setText(journeyInfo.getCarModel());
        holder.journey_start_tv.setText("Start Time & Date : "+ journeyInfo.getTime());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mCallback!=null)
                    mCallback.onItemClick(journeyInfo.get_id());
            }
        });

        Glide
                .with(context)
                .load(Uri.parse(journeyInfo.getImage_file()))
                .centerCrop()
                .thumbnail(0.1f)
                .placeholder(R.drawable.round_bg)
                .into(holder.profile_image);
    }

    @Override
    public int getItemCount() {
        int size = 0;
        if(mJourneyInfo!=null)
         size=mJourneyInfo.size();
        return size;
    }

    public class HistoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.status)
        TextView mStatus;
        @BindView(R.id.car_number_tv)
        TextView car_number_tv;
        @BindView(R.id.car_type_tv)
        TextView car_type_tv;
        @BindView(R.id.journey_start_tv)
        TextView journey_start_tv;
        @BindView(R.id.profile_image)
        CircleImageView profile_image;

        @BindDrawable(R.drawable.bg_pending)
        Drawable pending;
        @BindDrawable(R.drawable.bg_accepted)
        Drawable accepted;
        @BindDrawable(R.drawable.bg_started)
        Drawable started;
        @BindDrawable(R.drawable.bg_declined)
        Drawable declined;
        @BindDrawable(R.drawable.bg_completed)
        Drawable completed;

        public HistoryViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setOnItemClickListener(final JourneyItemClickListener listener) {
        mCallback = listener;
    }

    public interface JourneyItemClickListener {
        void onItemClick(String journeyID);
    }
}