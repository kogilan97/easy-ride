package com.finalyearproject.easyride.easyride.CarManagement.presenter;

import android.net.Uri;
import android.util.Log;

import com.finalyearproject.easyride.EasyRideApplication;
import com.finalyearproject.easyride.data.UserModalClass;
import com.finalyearproject.easyride.database.EasyRideStorage;
import com.finalyearproject.easyride.di.module.APIInterface;
import com.finalyearproject.easyride.di.module.RetrofitModule;
import com.finalyearproject.easyride.easyride.CarManagement.model.MainCarsClass;
import com.finalyearproject.easyride.easyride.CarManagement.view.DriverAddCarActivity;
import com.finalyearproject.easyride.easyride.CarManagement.view.DriverCarsFragment;
import com.finalyearproject.easyride.easyride.utils.FileUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.sql.Driver;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class CarsPresenterImpl implements CarsInterface.Presenter {
    DriverCarsFragment DriverCarsFragment;
    DriverAddCarActivity DriverAddCarActivity;
    APIInterface mAPIInterface;

    public CarsPresenterImpl(DriverCarsFragment view) {
        this.DriverCarsFragment = view;
        mAPIInterface = RetrofitModule.getApiInterface();
    }

    public CarsPresenterImpl(DriverAddCarActivity view) {
        this.DriverAddCarActivity = view;
        mAPIInterface = RetrofitModule.getApiInterface();
    }


    @Override
    public void onFetchDetails() {
        mAPIInterface.viewAllCars(EasyRideStorage.getInstance().getPrefUserId()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<MainCarsClass>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        DriverCarsFragment.showError(e.toString());
                    }

                    @Override
                    public void onNext(MainCarsClass MainCarsClass) {
                        DriverCarsFragment.onNext(MainCarsClass.getMessage(), MainCarsClass.getCardetails());
                    }

                });
    }


    @Override
    public void OnAddCarDetails(String carplateno, String carmodel, String pax, Uri uri) {

        JSONObject object = new JSONObject();
        try {
            object.put("userid", EasyRideStorage.getInstance().getPrefUserId());
            object.put("email", EasyRideStorage.getInstance().getPrefEmailAddr());
            object.put("carnumber", carplateno);
            object.put("carmodel", carmodel);
            object.put("capacity", pax);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("raw"), object.toString());

        mAPIInterface.addNewCar(body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<MainCarsClass>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        DriverAddCarActivity.showError(e.toString());
                    }

                    @Override
                    public void onNext(MainCarsClass mainCarsClass) {
                        uploadImage(mainCarsClass.getCarDetailsClass().getId(),uri);
                       // DriverAddCarActivity.onNext(mainCarsClass.getMessage(), mainCarsClass.getStatus());
                    }
                });
    }

    public void uploadImage(String id, Uri uri) {
        DriverAddCarActivity.showProgress();
        File file = new File(uri.getEncodedPath());
        if (uri.toString().contains("content:/")) {
            file = new File(FileUtil.getRealPathFromURI(EasyRideApplication.getContext(), uri));
        }

        // upload to backend first
        RequestBody fbody = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData("image_file", file.getName(), fbody);

        mAPIInterface.uploadCarImage(EasyRideStorage.getInstance().getAuthToken(), id,filePart)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<UserModalClass>() {
            @Override
            public void onCompleted() {
              //  DriverAddCarActivity.showComplete();
            }


            @Override
            public void onError(Throwable e) {
               // DriverAddCarActivity.hideProgress();
                DriverAddCarActivity.showError(e.getMessage());

            }

            @Override
            public void onNext(UserModalClass status) {
               // DriverAddCarActivity.hideProgress();
                if(status.getStatus()){
                    DriverAddCarActivity.onNext(status.getMessage(),status.getStatus());
                }

            }
        });
    }

    @Override
    public void onDeleteCar(String id) {
        JSONObject jsonObject = new JSONObject();


        mAPIInterface.deleteCar(id).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<MainCarsClass>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        DriverCarsFragment.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(MainCarsClass mainCarsClass) {
                        DriverCarsFragment.afterDelete(mainCarsClass.getMessage(), mainCarsClass.getStatus());
                    }
                });
    }
}
