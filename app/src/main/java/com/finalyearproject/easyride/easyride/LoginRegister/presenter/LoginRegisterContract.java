package com.finalyearproject.easyride.easyride.LoginRegister.presenter;

public interface LoginRegisterContract {

    interface Presenter{
        void onFetchDetails(String username, String password, String email, String phonenumber);
        void onFetchLoginDetails(String email, String password);
    }


    interface View {
        void onNext(String msg, Boolean status);

        void showError(String msg);
    }

}
