package com.finalyearproject.easyride.easyride.Driver.presenter;

import com.finalyearproject.easyride.data.UserModalClass;
import com.finalyearproject.easyride.database.EasyRideStorage;
import com.finalyearproject.easyride.di.module.APIInterface;
import com.finalyearproject.easyride.di.module.RetrofitModule;
import com.finalyearproject.easyride.easyride.CarManagement.model.MainCarsClass;
import com.finalyearproject.easyride.easyride.Driver.view.AddPathActivity;
import com.finalyearproject.easyride.easyride.Driver.view.DriverHomeFragment;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class PathPresenterImpl {

    DriverHomeFragment driverHomeFragment;
    AddPathActivity addPathActivity;
    APIInterface mAPIInterface;

    public PathPresenterImpl(DriverHomeFragment view) {
        this.driverHomeFragment = view;
        mAPIInterface = RetrofitModule.getApiInterface();
    }

    public PathPresenterImpl(AddPathActivity view) {
        this.addPathActivity = view;
        mAPIInterface = RetrofitModule.getApiInterface();
    }

    public void onFetchDetails() {
        mAPIInterface.getAllPath(EasyRideStorage.getInstance().getAuthToken(),EasyRideStorage.getInstance().getPrefUserId()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                       // driverHomeFragment.showError(e.toString());
                    }

                    @Override
                    public void onNext(UserModalClass userModalClass) {
                        driverHomeFragment.onPathFetched(userModalClass.getPathInfo());
                    }

                });
    }

    public void getCars(){
        mAPIInterface.viewAllCars(EasyRideStorage.getInstance().getPrefUserId()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<MainCarsClass>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                       // addPathActivity.showError(e.toString());
                    }

                    @Override
                    public void onNext(MainCarsClass MainCarsClass) {
                        addPathActivity.onCarsFetched(MainCarsClass.getMessage(), MainCarsClass.getCardetails());
                    }

                });
    }

    public void createPath(List<LatLng> latLngList, String dateTime, String pathName, String carID) throws JSONException {

        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        jsonObject.put("dateTime", dateTime);
        jsonObject.put("driverId",EasyRideStorage.getInstance().getPrefUserId());
        jsonObject.put("pathName", pathName);
        jsonObject.put("carId",carID);

        for(int i = 0; i<latLngList.size();i++){
            JSONObject waypointObject = new JSONObject();

            if(i == 0)
                waypointObject.put("isStart",true);
            else
                waypointObject.put("isStart",false);

            if(i==1)
                waypointObject.put("isEnd",true);
            else
                waypointObject.put("isEnd",false);

            waypointObject.put("longitude",String.valueOf(latLngList.get(i).longitude));
            waypointObject.put("latitude",String.valueOf(latLngList.get(i).latitude));

            jsonArray.put(waypointObject);
        }

        jsonObject.put("wayPoints",jsonArray);
        RequestBody body = RequestBody.create(MediaType.parse("raw"), jsonObject.toString());

        mAPIInterface.createPath(body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                      //  addPathActivity.showError(e.toString());
                    }

                    @Override
                    public void onNext(UserModalClass userModalClass) {
                         addPathActivity.onNext(userModalClass);
                    }
                });

    }
}
