package com.finalyearproject.easyride.easyride.Journey.model;

import com.finalyearproject.easyride.easyride.LoginRegister.model.UserInfo;

public class JourneyInfo {

    public String _id;
    public String dateTime;
    public String carId;
    public String driverResponse;
    public String userId;
    public String driverId ;
    public String journeyStatus;
    public String startPointLat ;
    public String startPointLong ;
    public String endPointLat ;
    public String endPointLong ;
    public String carNumber ;
    public String carModel ;
    public String createdAt ;
    public String updatedAt ;
    public DriverInfo driverInfo ;
    public PaymentInfo paymentInfo;
    public UserInfo userInfo;
    public CarInfo carInfo ;
    public String price;
    public String image_file;
    public String passengerId ;
    public String passsengerName;
    public String passengerPhone;

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public PaymentInfo getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(PaymentInfo paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    public String getPassengerId() {
        return passengerId;
    }

    public void setPassengerId(String passengerId) {
        this.passengerId = passengerId;
    }

    public String getPasssengerName() {
        return passsengerName;
    }

    public void setPasssengerName(String passsengerName) {
        this.passsengerName = passsengerName;
    }

    public String getPassengerPhone() {
        return passengerPhone;
    }

    public void setPassengerPhone(String passengerPhone) {
        this.passengerPhone = passengerPhone;
    }

    public String getImage_file() {
        return image_file;
    }

    public void setImage_file(String image_file) {
        this.image_file = image_file;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getStartPointLat() {
        return startPointLat;
    }

    public void setStartPointLat(String startPointLat) {
        this.startPointLat = startPointLat;
    }

    public String getStartPointLong() {
        return startPointLong;
    }

    public void setStartPointLong(String startPointLong) {
        this.startPointLong = startPointLong;
    }

    public String getEndPointLat() {
        return endPointLat;
    }

    public void setEndPointLat(String endPointLat) {
        this.endPointLat = endPointLat;
    }

    public String getEndPointLong() {
        return endPointLong;
    }

    public void setEndPointLong(String endPointLong) {
        this.endPointLong = endPointLong;
    }

    public DriverInfo getDriverInfo() {
        return driverInfo;
    }

    public void setDriverInfo(DriverInfo driverInfo) {
        this.driverInfo = driverInfo;
    }

    public CarInfo getCarInfo() {
        return carInfo;
    }

    public void setCarInfo(CarInfo carInfo) {
        this.carInfo = carInfo;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getTime() {
        return dateTime;
    }

    public void setTime(String time) {
        this.dateTime = time;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getDriverResponse() {
        return driverResponse;
    }

    public void setDriverResponse(String driverResponse) {
        this.driverResponse = driverResponse;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getJourneyStatus() {
        return journeyStatus;
    }

    public void setJourneyStatus(String journeyStatus) {
        this.journeyStatus = journeyStatus;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
