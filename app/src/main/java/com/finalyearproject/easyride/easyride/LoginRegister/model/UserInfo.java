package com.finalyearproject.easyride.easyride.LoginRegister.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserInfo  {
    @SerializedName("_id")
    private String id;
    @SerializedName("username")
    private String username;
    @SerializedName("password")
    private String password;
    @SerializedName("email")
    private String email;
    @SerializedName("createdAt")
    private String createdAt;
    @SerializedName("updatedAt")
    private String updatedAt;
    @SerializedName("emailverification")
    private Boolean emailverification;
    @SerializedName("phoneverification")
    private Boolean phoneverification;
    @SerializedName("approle")
    private String approle;
    @SerializedName("phone")
    private String phonenumber;
    @SerializedName("icverification")
    private Boolean icverification;
    @SerializedName("licenseverification")
    private Boolean licenseverification;


    public Boolean getICverification() {
        return icverification;
    }

    public void setICverification(Boolean icverification) {
        this.icverification = icverification;
    }

    public Boolean getLicenseverification() {
        return licenseverification;
    }

    public void setLicenseverification(Boolean licenseverification) {
        this.licenseverification = licenseverification;
    }


    public Boolean getEmailverification() {
        return emailverification;
    }

    public void setEmailverification(Boolean emailverification) {
        this.emailverification = emailverification;
    }

    public Boolean getPhoneverification() {
        return phoneverification;
    }

    public void setPhoneverification(Boolean phoneverification) {
        this.phoneverification = phoneverification;
    }

    public String getApprole() {
        return approle;
    }

    public void setApprole(String approle) {
        this.approle = approle;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }


}
