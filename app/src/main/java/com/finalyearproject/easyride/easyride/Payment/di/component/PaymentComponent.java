package com.finalyearproject.easyride.easyride.Payment.di.component;

import android.content.Context;

import com.finalyearproject.easyride.di.component.ApplicationComponent;
import com.finalyearproject.easyride.di.qualifier.ActivityContext;
import com.finalyearproject.easyride.di.scope.ActivityScope;
import com.finalyearproject.easyride.easyride.Payment.di.module.PaymentContextModule;
import com.finalyearproject.easyride.easyride.Payment.di.module.PaymentPresenterModule;
import com.finalyearproject.easyride.easyride.Payment.view.ActiveCardActivity;
import com.finalyearproject.easyride.easyride.Payment.view.AddCardActivity;
import com.finalyearproject.easyride.easyride.Payment.view.PaymentActivity;

import dagger.Component;

@ActivityScope
@Component(modules = {PaymentContextModule.class, PaymentPresenterModule.class}, dependencies = ApplicationComponent.class)
public interface PaymentComponent {
    @ActivityContext
    Context getContext();

    void injectAddCreditCardActivity(AddCardActivity addCardActivity);
    void injectActiveCardActivity(ActiveCardActivity activeCardActivity);
    void injectPaymentActivity(PaymentActivity paymentActivity);

}


