package com.finalyearproject.easyride.easyride.LoginRegister.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.finalyearproject.easyride.R;
import com.finalyearproject.easyride.easyride.LoginRegister.presenter.LoginPresenterImpl;
import com.finalyearproject.easyride.easyride.LoginRegister.presenter.LoginRegisterContract;
import com.finalyearproject.easyride.easyride.view.BaseActivity;

public class SignupActivity extends BaseActivity implements LoginRegisterContract.View {


    private TextView signintext;
    private Button submitButton;
    private TextView txtpass;
    private TextView txtconfirmpass;
    private TextView txtemail;
    private TextView txtphone;
    private TextView txtusername;

    LoginPresenterImpl mPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        mPresenter = new LoginPresenterImpl(this);


        signintext = findViewById(R.id.txtsignin);
        submitButton = findViewById(R.id.btnsignup);
        txtpass = findViewById(R.id.input_password);
        txtconfirmpass = findViewById(R.id.input_confirmpassword);
        txtemail = findViewById(R.id.input_email);
        txtusername = findViewById(R.id.input_username);
        txtphone = findViewById(R.id.input_phone_number);

        signintext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CannotBeEmpty()) {
                    if (ValidatePassword()) {
                        if (validateEmail(txtemail.getText()))
                            saveDetails();
                        else
                            Toast.makeText(getApplicationContext(), "Incorrect Email Address", Toast.LENGTH_SHORT).show();
                    } else
                        Toast.makeText(getApplicationContext(), "Password and Confirm Password did not match", Toast.LENGTH_SHORT).show();

                } else
                    Toast.makeText(getApplicationContext(), "Textfields cannot be empty", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void saveDetails() {
        mPresenter.onFetchDetails(txtusername.getText().toString(), txtpass.getText().toString(), txtemail.getText().toString(), txtphone.getText().toString());
    }


    public boolean validateEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public Boolean ValidatePassword() {
        return (txtpass.getText().toString().equals(txtconfirmpass.getText().toString()));
    }

    public Boolean CannotBeEmpty() {
        return (!TextUtils.isEmpty(txtusername.getText()) && !TextUtils.isEmpty(txtemail.getText()) && !TextUtils.isEmpty(txtpass.getText()) && !TextUtils.isEmpty(txtconfirmpass.getText()) && !TextUtils.isEmpty(txtphone.getText()));
    }


    @Override
    public void onNext(String msg, Boolean status) {
        if (!status) {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public void showError(String msg) {
        Log.d("TAG", msg);
    }
}
