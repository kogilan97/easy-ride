package com.finalyearproject.easyride.easyride.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.finalyearproject.easyride.R;
import com.finalyearproject.easyride.easyride.BookJourney.view.MapsActivity;
import com.skyfishjy.library.RippleBackground;

public class RiderHomeFragment extends Fragment {

    private Button btnfindride;
    private RippleBackground rippleBackground;

    public static RiderHomeFragment getInstance() {
        return new RiderHomeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        View view= inflater.inflate(R.layout.fragment_rider_home, container, false);

        btnfindride = view.findViewById(R.id.btnfindride);
        btnfindride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MapsActivity.class);
                startActivity(intent);
            }
        });

        rippleBackground = view.findViewById(R.id.content);
        rippleBackground.startRippleAnimation();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Rider Home");
    }


}
