package com.finalyearproject.easyride.easyride.CarManagement.presenter;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import androidx.fragment.app.Fragment;

import com.finalyearproject.easyride.EasyRideApplication;
import com.finalyearproject.easyride.database.EasyRideStorage;
import com.finalyearproject.easyride.di.module.APIInterface;
import com.finalyearproject.easyride.di.module.RetrofitModule;
import com.finalyearproject.easyride.easyride.utils.ChoosePhoto;

import java.io.File;

public class UploadImagePresenter {
    public static final int PICK_IMAGE = 3;

    private Uri mUrlPath;
    private View mView;
    private APIInterface mApiInterface;
    private ChoosePhoto mChoosePhoto;
    private String config;

    public UploadImagePresenter(View view) {
        this.mView = view;
        mApiInterface = RetrofitModule.getApiInterface("https://utjqmc4q39.execute-api.ap-southeast-1.amazonaws.com/");
    }

    public void init(Activity activity) {
        mChoosePhoto = new ChoosePhoto(activity);
    }

    public void onImageSelect(Activity fragment) {
        if (mChoosePhoto == null) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

            fragment.startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
        } else {
            mChoosePhoto.showAlertDialog(fragment);
        }
    }

    /**
     * Gets the content:// URI  from the given corresponding path to a file
     *
     * @param context
     * @param imageFile
     * @return content Uri
     */
    public static Uri getImageContentUri(Context context, java.io.File imageFile) {

        String filePath = imageFile.getAbsolutePath();
        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Images.Media._ID},
                MediaStore.Images.Media.DATA + "=? ",
                new String[]{filePath}, null);

        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
            Uri baseUri = Uri.parse("content://media/external/images/media");
            return Uri.withAppendedPath(baseUri, "" + id);
        } else {
            if (imageFile.exists()) {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.DATA, filePath);
                return context.getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            } else {
                return Uri.parse(imageFile.getAbsolutePath());
            }
        }

    }


    public void setUrlPath(Uri url) {
        mUrlPath = getImageContentUri(EasyRideApplication.getContext(), new File(url.getEncodedPath()));
        mView.updateImageView(mUrlPath);
    }

    public void handleCameraResult(Uri url) {
        mUrlPath = getImageContentUri(EasyRideApplication.getContext(), new File(url.getEncodedPath()));
        mView.updateImageView(mUrlPath);
    }

    public void handleGalleryResult(Intent data) {
        mChoosePhoto.handleGalleryResult(data);
    }

    public void handleGalleryResult(Intent data, Fragment fragment) {
        mChoosePhoto.handleGalleryResult(data, fragment);
    }

    public void handleGalleryResult(Intent data, Activity fragment) {
        mChoosePhoto.handleGalleryResult(data, fragment);
    }


    public void handleCropResult() {
        mUrlPath = mChoosePhoto.getCropImageUrl();
        mView.updateImageView(mUrlPath);
    }

    public interface View {

        void onError(String error);

        void showProgress();

        void dismissProgress();

        void updateImageView(Uri uri);

        void onContinueAction();
    }
}
