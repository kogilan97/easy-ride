package com.finalyearproject.easyride.easyride.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

public class BaseActivity extends AppCompatActivity {

    private FragmentManager mFragmentManager;

    public FragmentManager getSafeFragmentManager() {
        if (this.mFragmentManager == null) {
            this.mFragmentManager = this.getSupportFragmentManager();
        }
        return this.mFragmentManager;
    }
}
