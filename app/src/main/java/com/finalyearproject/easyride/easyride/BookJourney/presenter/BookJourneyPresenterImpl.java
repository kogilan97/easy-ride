package com.finalyearproject.easyride.easyride.BookJourney.presenter;

import com.finalyearproject.easyride.data.UserModalClass;
import com.finalyearproject.easyride.database.EasyRideStorage;
import com.finalyearproject.easyride.di.module.APIInterface;
import com.finalyearproject.easyride.di.module.RetrofitModule;
import com.finalyearproject.easyride.easyride.BookJourney.view.MapsActivity;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class BookJourneyPresenterImpl implements BookJourneyContract.Presenter {

    private APIInterface mApiInterface;
    private MapsActivity mapsActivity;

    public BookJourneyPresenterImpl(MapsActivity mapsActivity){
        this.mapsActivity = mapsActivity;
        mApiInterface = RetrofitModule.getApiInterface();
    }

    @Override
    public void getPath(String id, Marker marker, String price) {
        mApiInterface.getPathInfo(id).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {
                    @Override
                    public void onCompleted() {
                        mapsActivity.showComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mapsActivity.showError(String.valueOf(e));
                    }

                    @Override
                    public void onNext(UserModalClass status) {
                        mapsActivity.hideProgress();
                        mapsActivity.onShowPathDetails(status.getMessage(), status.getPath(),marker,price);
                    }
                });
    }

    @Override
    public void searchPath(LatLng start, LatLng end, String date) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("dateTime", date);
            jsonObject.put("startLatitude", String.valueOf(start.latitude));
            jsonObject.put("startLongitude", String.valueOf(start.longitude));
            jsonObject.put("endLatitude", String.valueOf(end.latitude));
            jsonObject.put("endLongitude", String.valueOf(end.longitude));
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        RequestBody body =RequestBody.create(MediaType.parse("raw"),jsonObject.toString());
        mApiInterface.getPath(body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserModalClass>() {
                    @Override
                    public void onCompleted() {
                        mapsActivity.showComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mapsActivity.showError(String.valueOf(e));
                    }

                    @Override
                    public void onNext(UserModalClass status) {
                        mapsActivity.hideProgress();
                        mapsActivity.onShowPathList(status.getMessage(), status.getPaths());
                    }
                });


    }

    public void getPricing(Marker marker, double startLatitude, double startLongitude, double endLatitude, double endLongitude){
        JSONObject jsonObject = new JSONObject();
        try {
        jsonObject.put("startLat", startLatitude);
        jsonObject.put("startLong", startLongitude);
        jsonObject.put("endLat", endLatitude);
        jsonObject.put("endLong", endLongitude);

            RequestBody body = RequestBody.create(MediaType.parse("raw"),jsonObject.toString());
            mApiInterface.getPricing(EasyRideStorage.getInstance().getAuthToken(),body).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<UserModalClass>() {
                        @Override
                        public void onCompleted() {
                            mapsActivity.showComplete();
                        }

                        @Override
                        public void onError(Throwable e) {
                            mapsActivity.showError(String.valueOf(e));
                        }

                        @Override
                        public void onNext(UserModalClass status) {
                            mapsActivity.hideProgress();
                            mapsActivity.onGetPricing(marker,status.getTotalCost());
                        }
                    });

        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
    }

    @Override
    public void createJourney(String formattedDateForBackend, String carId, String driverId, double startLatitude, double startLongitude, double endLatitude, double endLongitude) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("dateTime", formattedDateForBackend);
            jsonObject.put("carId", carId);
            jsonObject.put("driverId", driverId);
            jsonObject.put("userId", EasyRideStorage.getInstance().getPrefUserId());
            jsonObject.put("startPointLat", startLatitude);
            jsonObject.put("startPointLong", startLongitude);
            jsonObject.put("endPointLat", endLatitude);
            jsonObject.put("endPointLong", endLongitude);

            RequestBody body =RequestBody.create(MediaType.parse("raw"),jsonObject.toString());
            mApiInterface.createJourney(body).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<UserModalClass>() {
                        @Override
                        public void onCompleted() {
                            mapsActivity.showComplete();
                        }

                        @Override
                        public void onError(Throwable e) {
                            mapsActivity.showError(String.valueOf(e));
                        }

                        @Override
                        public void onNext(UserModalClass status) {
                            mapsActivity.hideProgress();
                            mapsActivity.onCreateJourney(status.getJourneyInfo());
                        }
                    });

        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
    }
}
