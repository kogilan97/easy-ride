package com.finalyearproject.easyride.easyride.Driver.view;

import android.app.AlertDialog;
import android.location.Address;
import android.location.Geocoder;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import com.finalyearproject.easyride.R;
import com.finalyearproject.easyride.data.UserModalClass;
import com.finalyearproject.easyride.easyride.BookJourney.model.Path;
import com.finalyearproject.easyride.easyride.CarManagement.model.CarDetailsClass;
import com.finalyearproject.easyride.easyride.Driver.presenter.PathPresenterImpl;
import com.finalyearproject.easyride.easyride.Driver.utils.DirectionPointListener;
import com.finalyearproject.easyride.easyride.Driver.utils.GetPathFromLocation;
import com.finalyearproject.easyride.easyride.utils.Utils;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

public class AddPathActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private MapView mapView;
    private LatLngBounds bounds;
    private int zoomLevel;
    private LatLng start,end;
    private List<LatLng> latLngList;
    private Button btn_add_path;
    private EditText et_path_name;
    private Spinner gm_1;
    private ImageView date_picker;
    private String carID;
    private PathPresenterImpl mPresenter;
    private View dialogView;
    private EditText et_path_date;
    private String final_date;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_add_path);
        mPresenter= new PathPresenterImpl(this);
        gm_1 = findViewById(R.id.gm_1);
        mapView = findViewById(R.id.map);
        mapView = findViewById(R.id.map);
        et_path_name = findViewById(R.id.et_path_name);
        btn_add_path = findViewById(R.id.btn_add_path);
        mapView.onCreate(savedInstanceState);
        et_path_date= findViewById(R.id.et_path_date);
        Date currentdate = new Date();
        et_path_date.setText(Utils.getFormattedDateForBackend(Calendar.getInstance().getTime()));

        date_picker = findViewById(R.id.date_picker_add_path_icon);
        mapView.onResume(); // needed to get the map to display immediately

         dialogView = View.inflate(this, R.layout.date_time_picker, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
     date_picker.setOnClickListener(new View.OnClickListener()
     { @Override
     public void onClick(View view){

         dialogView.findViewById(R.id.date_time_set).setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 DatePicker datePicker = (DatePicker) dialogView.findViewById(R.id.date_picker);
                 TimePicker timePicker = (TimePicker) dialogView.findViewById(R.id.time_picker);

                 Calendar calendar = new GregorianCalendar(datePicker.getYear(),
                         datePicker.getMonth(),
                         datePicker.getDayOfMonth(),
                         timePicker.getCurrentHour(),
                         timePicker.getCurrentMinute());

                 final_date = Utils.getFormattedDateForBackend(calendar.getTime());
                 et_path_date.setText(final_date);
                 alertDialog.dismiss();
             }});

         alertDialog.setView(dialogView);
         alertDialog.show();

     }
     });




        start = (LatLng) getIntent().getExtras().getParcelable("start");
        end = (LatLng) getIntent().getExtras().getParcelable("end");
        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(start!=null && end != null)
            mapView.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        latLngList = new ArrayList<>();
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                LatLng source = new LatLng(start.latitude,start.longitude);
                mMap.addMarker(new MarkerOptions().position(source));
                builder.include(source);

                LatLng destination = new LatLng(end.latitude,end.longitude);
                mMap.addMarker(new MarkerOptions().position(destination));
                builder.include(destination);

                new GetPathFromLocation(source, destination, new DirectionPointListener() {
                    @Override
                    public void onPath(PolylineOptions polyLine) {
                        mMap.addPolyline(polyLine);
                    }
                },mMap).execute();

                LatLngBounds bounds = builder.build();
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 0);
                mMap.animateCamera(cameraUpdate);
                mMap.getUiSettings().setZoomControlsEnabled(false);
                mMap.getUiSettings().setZoomGesturesEnabled(false);
                mMap.setLatLngBoundsForCameraTarget(bounds);
                latLngList.add(source);
                latLngList.add(destination);
            }
        });

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            int counter = 0;

            @Override
            public void onMapClick(LatLng latLng) {

                if(counter<5) {
                    Marker marker = mMap.addMarker(new MarkerOptions().position(latLng));
                    LinearLayout layout = (LinearLayout)findViewById(R.id.layout);
                    View child = getLayoutInflater().inflate(R.layout.view_child, null);
                    EditText stopEditText = (EditText) child.findViewById(R.id.stopEditText);
                    stopEditText.setText(getCompleteAddressString(latLng.latitude,latLng.longitude));
                    ImageView remove_stop = (ImageView) child.findViewById(R.id.remove_stop);
                    remove_stop.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                              marker.remove();
                              layout.removeView(child);
                              latLngList.remove(latLng);
                              counter = counter-1;
                        }
                    });
                    counter = counter+1;
                    latLngList.add(latLng);
                    layout.addView(child);



                }else{
                    Toast.makeText(AddPathActivity.this,"Maximum stops added",Toast.LENGTH_SHORT).show();
                }
            }
        });

        btn_add_path.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(carID !=null) {
                    try {
                        mPresenter.createPath(latLngList, final_date, et_path_name.getText().toString(),carID);
                        Toast.makeText(AddPathActivity.this,"Path has been successfully added.",Toast.LENGTH_SHORT).show();
                        finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.w("Current address", strReturnedAddress.toString());
            } else {
                Log.w("Current address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("Current address", "Cannot get Address!");
        }
        return strAdd;
    }

    public void onNext(UserModalClass userModalClass){
        if(userModalClass.getStatus())
            Toast.makeText(this,userModalClass.getMessage(),Toast.LENGTH_LONG).show();

        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mapView != null)
            mapView.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mapView != null)
            mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mapView != null)
            mapView.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mapView != null)
            mapView.onStart();

        mPresenter.getCars();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mapView != null) {
            mapView.onLowMemory();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mapView != null)
            mapView.onSaveInstanceState(outState);
    }

    public void onCarsFetched(String message, List<CarDetailsClass> cardetails) {
        ArrayAdapter<CarDetailsClass> spinnerArrayAdapter = new ArrayAdapter<CarDetailsClass>(
                this, android.R.layout.simple_spinner_item, cardetails);
        spinnerArrayAdapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );
        gm_1.setAdapter(spinnerArrayAdapter);

        CarDetailsClass initial = cardetails.get(0);
        carID = initial.getId();

        gm_1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                CarDetailsClass item = cardetails.get(i);
                carID = item.getId();
            }
        });
    }
}
