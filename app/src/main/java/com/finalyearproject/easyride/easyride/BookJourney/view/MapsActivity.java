package com.finalyearproject.easyride.easyride.BookJourney.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.finalyearproject.easyride.R;
import com.finalyearproject.easyride.easyride.BookJourney.model.Path;
import com.finalyearproject.easyride.easyride.BookJourney.presenter.BookJourneyContract;
import com.finalyearproject.easyride.easyride.BookJourney.presenter.BookJourneyPresenterImpl;
import com.finalyearproject.easyride.easyride.BookJourney.presenter.LocationPresenter;
import com.finalyearproject.easyride.easyride.Driver.utils.DirectionPointListener;
import com.finalyearproject.easyride.easyride.Driver.utils.GetPathFromLocation;
import com.finalyearproject.easyride.easyride.Journey.model.JourneyInfo;
import com.finalyearproject.easyride.easyride.Journey.view.JourneyDetailActivity;
import com.finalyearproject.easyride.easyride.utils.ClearableEditText;
import com.finalyearproject.easyride.easyride.utils.CustomizedSlideUpBuilder;
import com.finalyearproject.easyride.easyride.utils.PermissionCore;
import com.finalyearproject.easyride.easyride.utils.Utils;
import com.finalyearproject.easyride.easyride.view.BaseActivity;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.maps.android.ui.IconGenerator;
import com.mancj.slideup.SlideUp;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.finalyearproject.easyride.EasyRideApplication.getContext;

public class MapsActivity extends BaseActivity implements LocationPresenter.View, ViewPager.OnPageChangeListener, OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener, GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener, GoogleMap.OnCameraMoveListener,
        GoogleMap.OnCameraIdleListener, GoogleMap.OnInfoWindowClickListener, LocationSearchAdapter.ClickListener, BookJourneyContract.View {

    private SlideUp mCalendarSlider;
    private SlideUp mAddressSlider;

    @BindView(R.id.dim)
    View mDimView;
    @BindView(R.id.search_location_view)
    View mAddressSliderView;
    @BindView(R.id.txtCurrentLocation)
    TextView txtCurrentLocation;
    @BindView(R.id.txtStartDateGlobal)
    TextView txtStartDateGlobal;
    @BindView(R.id.txtStartTimeGlobal)
    TextView txtStartTimeGlobal;
    @BindView(R.id.date_picker)
    ImageView date_picker;
    @BindView(R.id.calendar_container)
    View calendar_container;

    private Date mStartDate = Utils.atDefaultStartTime(new Date(System.currentTimeMillis()));
    private Date mEndDate = Utils.atDefaultEndTime(mStartDate);
    private LatLng mCurrentLatLng;

    // filter by search
    public PlacesClient mPlacesClient;
    private LocationSearchAdapter mLocationSearchAdapter;
    private static LatLngBounds BOUNDS_WORLD = new LatLngBounds(new LatLng(-85, 180), new LatLng(85, -180));
    private RecyclerView location_recycler;
    private Path mPath;
    private LatLng endPoint;

    Calendar finalDate;

    // location
    private LocationPresenter mLocationPresenter;
    private Handler mHandler = new Handler(Looper.getMainLooper());

    private GoogleMap mGoogleMap;
    private static float DEFAULT_ZOOM_LEVEL = 14f;
    private static final Drawable TRANSPARENT_DRAWABLE = new ColorDrawable(Color.TRANSPARENT);
    private static final int REQUEST_CURRENTLOC = 893;
    boolean mRequestedGPS = false;
    AlertDialog mDialogGPS;
    MapView mapView;
    LatLng initCoordinates;

    BookJourneyPresenterImpl mPresenter;

    HashMap<String, Path> mMarkers = new HashMap<>();
    String[] MAPS_PERMISSIONS = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

    private boolean isKeyboardShown;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);

        Places.initialize(this,getString(R.string.google_places_api_key));
        mPlacesClient = Places.createClient(this);
        mPresenter= new BookJourneyPresenterImpl(this);

        mapView = findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();

        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        initSliderLocation();
        //initCalendarSlider();
        mLocationPresenter = new LocationPresenter(this, this);

        mLocationSearchAdapter = new LocationSearchAdapter(this);
        mLocationSearchAdapter.setClickListener(this);
        location_recycler.setAdapter(mLocationSearchAdapter);
        mLocationSearchAdapter.notifyDataSetChanged();

        final View dialogView = View.inflate(this, R.layout.date_time_picker, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        date_picker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogView.findViewById(R.id.date_time_set).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DatePicker datePicker = (DatePicker) dialogView.findViewById(R.id.date_picker);
                        TimePicker timePicker = (TimePicker) dialogView.findViewById(R.id.time_picker);

                        Calendar calendar = new GregorianCalendar(datePicker.getYear(),
                                datePicker.getMonth(),
                                datePicker.getDayOfMonth(),
                                timePicker.getCurrentHour(),
                                timePicker.getCurrentMinute());

                        finalDate = calendar;
                        calendar_container.setVisibility(View.VISIBLE);
                        TextView date = (TextView) calendar_container.findViewById(R.id.txtStartDateGlobal);
                        TextView time = (TextView) calendar_container.findViewById(R.id.txtStartTimeGlobal);
                        date.setText("Date : " +Utils.getFormattedDate(calendar.getTime()));
                        time.setText("Time : "+ Utils.getFormattedTime(calendar.getTime()));
                        alertDialog.dismiss();
                    }});
                alertDialog.setView(dialogView);
                alertDialog.show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mapView != null)
            mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mapView != null)
            mapView.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mapView != null)
            mapView.onStart();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mapView != null) {
            mapView.onLowMemory();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public void updateLocationText(String text) {
        txtCurrentLocation.setText(text);
    }

    private void initSliderLocation() {
        CustomizedSlideUpBuilder slideUpBuilder = new CustomizedSlideUpBuilder(mAddressSliderView, mDimView);
        mAddressSlider = slideUpBuilder.build();
        mAddressSlider.addSlideListener(slideUpBuilder.getSlideUpListener());

        TextView closeBtn = mAddressSliderView.findViewById(R.id.close_btn);
        closeBtn.setOnClickListener(v -> closeSliderUp(v, mAddressSlider));

        location_recycler = mAddressSliderView.findViewById(R.id.location_recycler);
        location_recycler.setLayoutManager(new LinearLayoutManager(this));
        Button btn = mAddressSliderView.findViewById(R.id.btn_start);

        ClearableEditText startSearchEditText = mAddressSliderView.findViewById(R.id.startSearchEditText);
        ClearableEditText endSearchEditText = mAddressSliderView.findViewById(R.id.endSearchEditText);

        startSearchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        endSearchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        startSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (mLocationSearchAdapter == null) {
                    return;
                }
                if (start < 4) {
                    mLocationSearchAdapter.notifyDataSetChanged();
                    return;
                }
                if (mLocationSearchAdapter != null) {
                    if (!s.toString().equals("")) {
                        mLocationSearchAdapter.getFilter().filter(s.toString());
                        if (location_recycler.getVisibility() == View.GONE) {location_recycler.setVisibility(View.VISIBLE);}
                    } else {
                        if (location_recycler.getVisibility() == View.VISIBLE) {location_recycler.setVisibility(View.GONE);}
                    }
                }

                if (count == 0) {
                    if (mLocationSearchAdapter != null) {
                        mLocationSearchAdapter.clear();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (startSearchEditText.getText().toString().isEmpty()) {
                    if (mLocationSearchAdapter != null) {
                        mLocationSearchAdapter.clear();
                    }
                }
            }
        });


        endSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (mLocationSearchAdapter == null)
                    return;

                if (start < 4) {
                    mLocationSearchAdapter.notifyDataSetChanged();
                    return;
                }
                if (mLocationSearchAdapter != null) {
                    if (!s.toString().equals("")) {
                        mLocationSearchAdapter.getFilter().filter(s.toString());
                        if (location_recycler.getVisibility() == View.GONE) {location_recycler.setVisibility(View.VISIBLE);}
                    } else
                        if (location_recycler.getVisibility() == View.VISIBLE) {location_recycler.setVisibility(View.GONE);}
                }

                if (count == 0)
                    if (mLocationSearchAdapter != null)
                        mLocationSearchAdapter.clear();
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (endSearchEditText.getText().toString().isEmpty())
                    if (mLocationSearchAdapter != null)
                        mLocationSearchAdapter.clear();
            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("TAG1","Entering Layer 1");
                Log.d("TAG1",startSearchEditText.getText().toString());


                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                LatLng source = new LatLng((double) getLocationFromAddress(startSearchEditText.getText().toString()).latitude,(double) getLocationFromAddress(startSearchEditText.getText().toString()).longitude);
                mGoogleMap.addMarker(new MarkerOptions().position(source));
                builder.include(source);

                LatLng destination = new LatLng((double) getLocationFromAddress(endSearchEditText.getText().toString()).latitude,(double) getLocationFromAddress(endSearchEditText.getText().toString()).longitude);
                mGoogleMap.addMarker(new MarkerOptions().position(destination));
                builder.include(destination);

                endPoint = destination;

                new GetPathFromLocation(source, destination, new DirectionPointListener() {
                    @Override
                    public void onPath(PolylineOptions polyLine) {
                        mGoogleMap.addPolyline(polyLine);
                    }
                },mGoogleMap).execute();

                mGoogleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
                        if (mGoogleMap != null)
                            if (mPresenter != null)
                                mPresenter.searchPath(source,destination,Utils.getFormattedDateForBackend(finalDate.getTime()));
                    }
                });

                LatLngBounds bounds = builder.build();
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 50);
                mGoogleMap.animateCamera(cameraUpdate);
                mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
                mGoogleMap.getUiSettings().setZoomGesturesEnabled(false);
                mGoogleMap.setLatLngBoundsForCameraTarget(bounds);

                closeSliderUp(view, mAddressSlider);
            }
        });


        TextView txtMyLocation = mAddressSliderView.findViewById(R.id.txtMyLocation);
        txtMyLocation.setOnClickListener(v -> {
            closeSliderUp(v, mAddressSlider);
            onPermissionSuccess(); // call main presenter for location
        });

    }public LatLng getLocationFromAddress(String strAddress){

        Geocoder coder = new Geocoder(this);
        List<Address> address;
        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(strAddress,5);
            if (address==null)
                return null;
            Address location=address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude());

            return p1;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return p1;
    }



    private void closeSliderUp(View view, SlideUp slideUp) {
        if (slideUp.isVisible()) {
            if (isKeyboardShown) {
                InputMethodManager in = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(view.getWindowToken(), 0);

            } else
                slideUp.hide();
        }
    }

    @OnClick({R.id.filter_location_ll})
    public void onClick(View view) {
        if (view.getId() == R.id.filter_location_ll)
            mAddressSlider.show();
    }

    @OnClick(R.id.dim)
    public void onDimClicked(View view) {
        if (mAddressSlider.isVisible()) {
            if (isKeyboardShown) {
                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(view.getWindowToken(), 0);
            } else
                mAddressSlider.hide();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLocationPresenter.onDestroyView();
        if (mapView != null)
            mapView.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (mLocationPresenter != null)
            mLocationPresenter.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_CURRENTLOC)
            onMyLocationClick(null);
    }



    protected void showToastMessage(String message) {
        try {
            if (message != null)
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void moveToPointSmooth(LatLng latLng) {
        if (latLng != null & mGoogleMap != null) {
            float zoomLevel = DEFAULT_ZOOM_LEVEL;
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel);
            mGoogleMap.animateCamera(cameraUpdate);
        }
    }

    private void moveToPointSmooth(LatLng latLng, float zoom) {
        if (latLng != null) {
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, zoom);
            mGoogleMap.animateCamera(cameraUpdate);
        }
    }

    private void showSearchMapMarker(LatLng latLng) {
        MarkerOptions marker = new MarkerOptions().position(latLng);
        if (mGoogleMap != null)
            mGoogleMap.addMarker(marker);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mapView != null)
            mapView.onSaveInstanceState(outState);
    }

    private void addMarkersToMap(List<Path> paths) {
        if (mMarkers == null)
            mMarkers = new HashMap<>();


        for (Path path : paths) {
            if (path.getLatitude() == null || path.getLatitude() == null)
                continue;

                MarkerOptions markerOpt = new MarkerOptions().position(new LatLng(Double.valueOf(path.getLatitude()),Double.valueOf(path.getLongitude())));
                markerOpt.icon(BitmapDescriptorFactory.fromBitmap(getGeneratedMarker(path.getPathId())));
                markerOpt.snippet(String.valueOf(path.get_id()));
                Marker marker = mGoogleMap.addMarker(markerOpt);

                if (!mMarkers.containsKey(marker.getId()))
                    mMarkers.put(marker.getId(), path);

        }
    }

    private Bitmap getGeneratedMarker(String name) {
        LayoutInflater myInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View markerView = myInflater.inflate(R.layout.layout_maker, null, false);

        IconGenerator iconGenerator = new IconGenerator(getContext());
        iconGenerator.setContentView(markerView);
        iconGenerator.setColor(Color.TRANSPARENT);
        iconGenerator.setBackground(TRANSPARENT_DRAWABLE);

        return iconGenerator.makeIcon();
    }

    private boolean containPoints(Path path) {
        if (mMarkers == null)
            return false;

        return mMarkers.containsValue(path);
    }

    public void onMyLocationClick(View view) {

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, MAPS_PERMISSIONS, REQUEST_CURRENTLOC);
            return;
        }
        Location location = mLocationPresenter.getLastKnownLocation();

        if (location == null) {
            showToastMessage("Location disabled");
            return;
        }
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();

        moveToPointSmooth(new LatLng(latitude, longitude), 17);
    }

    private void initLocation() {
        try {
            if (PermissionCore.isGPSAvailable(getContext()))
                if (mLocationPresenter != null)
                    mLocationPresenter.checkForGPS();
        } catch (Settings.SettingNotFoundException e) {
            showToastMessage(e.getMessage());
            e.printStackTrace();
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onLocationChanged(Location location) {
        if (initCoordinates != null) {
            moveToPointSmooth(initCoordinates);
            initCoordinates = null;
            return;
        }
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 10f);

        if (this == null)
            return;

        if (mGoogleMap != null) {
            mGoogleMap.animateCamera(cameraUpdate);
            mLocationPresenter.stopLocationUpdates();
        }
    }

    @Override
    public void onPermissionSuccess() {
        mHandler.post(() -> {
            // we gonna get location here
            if (mLocationPresenter != null) {
                Location location = mLocationPresenter.getLastKnownLocation();

                try {
                    Address address = mLocationPresenter.getLocationAddress(location);
                    if (address != null) {
                        String locationAddress = address.getAdminArea();
                        if (address.getSubLocality() != null)
                            locationAddress = address.getSubLocality();
                        else if (address.getThoroughfare() != null)
                            locationAddress = address.getThoroughfare();
                        else if (address.getLocality() != null)
                            locationAddress = address.getLocality();

                        updateLocationText(locationAddress);
                        mCurrentLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        initLocation();

        if (mapView != null)
            mapView.getMapAsync(this);

        if (mGoogleMap != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                return;

            mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
            mGoogleMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onPermissionFailed() {
        mLocationPresenter.checkPermissions(this);
    }

    @Override
    public void locationUnavailable() {
        showToastMessage("Location settings are inadequate, and cannot be fixed here. Dialog not created.");
        moveToPointSmooth(initCoordinates);
    }

    @Override
    public void locationSuccess(LocationSettingsResult result) {
        mLocationPresenter.initLocationUpdates();
    }

    @Override
    public void resolutionRequired(LocationSettingsResult result) {
        final Status status = result.getStatus();
        moveToPointSmooth(initCoordinates);
        try {
            status.startResolutionForResult(this, PermissionCore.RC_GPS_ON);
        } catch (IntentSender.SendIntentException e) {
            showToastMessage("PendingIntent unable to execute request.");
        }
    }

    @Override
    public void onCameraMove() {
        CameraPosition position = mGoogleMap.getCameraPosition();
        Log.d("debug", "position.zoom = " + position.zoom);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        if(finalDate!=null)
            mPresenter.createJourney(Utils.getFormattedDateForBackend(finalDate.getTime()),mPath.getCarId(),mPath.getDriverId(),marker.getPosition().latitude,marker.getPosition().longitude,endPoint.latitude,endPoint.longitude);
        else
            Toast.makeText(this,"Please select date and time", Toast.LENGTH_SHORT).show();
    }

    public void onCreateJourney(JourneyInfo journey){
        finish();
        Intent intent = new Intent(MapsActivity.this,JourneyDetailActivity.class);
        intent.putExtra("journeyID",journey.get_id());
        startActivity(intent);
    }

    @Override
    public void onMapLongClick(LatLng latLng) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {

    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap map) {
        mGoogleMap = map;

        UiSettings mapSettings = mGoogleMap.getUiSettings();

        mapSettings.setCompassEnabled(false);
        mapSettings.setZoomControlsEnabled(false);
        mapSettings.setRotateGesturesEnabled(false);
        mapSettings.setMapToolbarEnabled(false);
        mapSettings.setTiltGesturesEnabled(false);

        if (initCoordinates == null)
            initCoordinates = new LatLng(3.141767, 101.686830);

        mGoogleMap.setMyLocationEnabled(true);
        initLocation();

        if (initCoordinates != null) {
            moveToPointSmooth(initCoordinates);
            initCoordinates = null;
        } else
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(3.141767, 101.686830), 10f));

        mGoogleMap.setOnCameraMoveListener(this);
        mGoogleMap.setOnCameraIdleListener(this);
        mGoogleMap.setOnMapLongClickListener(this);

        onMyLocationClick(null);

        // location layer
        map.setMyLocationEnabled(false);

        mGoogleMap.setOnMarkerClickListener(marker -> {
            if(mMarkers.get(marker.getId())!=null) {
                mPresenter.getPricing(marker,marker.getPosition().latitude,marker.getPosition().longitude,endPoint.latitude,endPoint.longitude);
            }

            return true;
        });
    }

    public String getAddress(double lat, double lng) throws IOException {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());

        String locationAddress = null;
        List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);

        if (addresses.size() > 0) {
            Address address = addresses.get(0);

            if (address != null) {
                locationAddress = address.getAdminArea();
                if (address.getSubLocality() != null)
                    locationAddress = address.getSubLocality();
                else if (address.getThoroughfare() != null)
                    locationAddress = address.getThoroughfare();
                 else if (address.getLocality() != null)
                    locationAddress = address.getLocality();
            } else
                locationAddress = "Cannot get location";
        }
        return locationAddress;
    }

    public void animateMarker(final Marker marker, final Location location) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final LatLng startLatLng = marker.getPosition();
        final double startRotation = marker.getRotation();
        final long duration = 500;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);

                double lng = t * location.getLongitude() + (1 - t)
                        * startLatLng.longitude;
                double lat = t * location.getLatitude() + (1 - t)
                        * startLatLng.latitude;

                float rotation = (float) (t * location.getBearing() + (1 - t)
                        * startRotation);

                marker.setPosition(new LatLng(lat, lng));
                marker.setRotation(rotation);

                if (t < 1.0)
                    handler.postDelayed(this, 16);
            }
        });
    }

    @Override
    public void onCameraIdle() {

    }

    public void onMapMove(String favourite) {
        updateLocationText(favourite);
    }

    @Override
    public void click(Place place) {
        ClearableEditText startSearchEditText = mAddressSliderView.findViewById(R.id.startSearchEditText);
        ClearableEditText endSearchEditText = mAddressSliderView.findViewById(R.id.endSearchEditText);
        if(startSearchEditText.isFocused())
            startSearchEditText.setText(place.getAddress());
        else if (endSearchEditText.isFocused())
            endSearchEditText.setText(place.getAddress());
    }

    @Override
    public void onShowPathList(String message, List<Path> path) {
        addMarkersToMap(path);
    }

    @Override
    public void onShowPathDetails(String message, Path path, Marker marker,String price) {
        this.mPath = path;
        mGoogleMap.setInfoWindowAdapter(new InfoWindowCustom(getContext(), mMarkers,path,price));
        mGoogleMap.setOnInfoWindowClickListener(this);
        marker.showInfoWindow();
    }

    @Override
    public void onNext(String message) {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void showComplete() {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    public void onGetPricing(Marker marker, String totalCost) {
        mPresenter.getPath(mMarkers.get(marker.getId()).getPathId(), marker,totalCost);

    }
}
