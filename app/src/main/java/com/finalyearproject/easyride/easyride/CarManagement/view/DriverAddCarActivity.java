package com.finalyearproject.easyride.easyride.CarManagement.view;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.finalyearproject.easyride.R;
import com.finalyearproject.easyride.easyride.CarManagement.presenter.CarsInterface;
import com.finalyearproject.easyride.easyride.CarManagement.presenter.CarsPresenterImpl;
import com.finalyearproject.easyride.easyride.CarManagement.presenter.UploadImagePresenter;
import com.finalyearproject.easyride.easyride.utils.ChoosePhoto;
import com.finalyearproject.easyride.easyride.view.BaseActivity;
import com.finalyearproject.easyride.easyride.view.DriverMainActivity;

public class DriverAddCarActivity extends BaseActivity implements CarsInterface.AddCarView, UploadImagePresenter.View {

    private ImageView btnback;
    private TextView txtcarmodel;
    private TextView txtcarnumber;
    private TextView txtnumberofpax;
    private Button savebutton;
    private ImageView img_add_car;

    private Uri mUri;
    Handler mHandler = new Handler(Looper.getMainLooper());
    private boolean checkImage = false;
    CarsPresenterImpl mPresenter;
    UploadImagePresenter uploadImagePresenter;

    public DriverAddCarActivity() {
        uploadImagePresenter = new UploadImagePresenter(this);
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_car);

        btnback = (ImageView) findViewById(R.id.ImageEditProfileBack);
        txtcarmodel = findViewById(R.id.input_car_model);
        txtcarnumber = findViewById(R.id.input_car_registration_number);
        txtnumberofpax = findViewById(R.id.input_car_capacity);
        savebutton = findViewById(R.id.btn_car_add);
        img_add_car = findViewById(R.id.img_add_car);

        mPresenter = new CarsPresenterImpl(this);
        uploadImagePresenter.init(this);

        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        savebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddDetails();
            }
        });

        img_add_car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageSelectOption();
            }
        });
    }

    private void imageSelectOption() {
        try {
            PackageManager pm = getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {"Choose From Gallery", "Cancel"};
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Select Option");
                builder.setItems(options, (dialog, item) -> {
                    if (options[item].equals("Choose From Gallery")) {
                        dialog.dismiss();
                        uploadImagePresenter.onImageSelect(this);

                    } else if (options[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                });
                builder.show();
            } else
                Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == ChoosePhoto.SELECT_PICTURE_CAMERA) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (data == null) {
            return;
        }

        if (requestCode == ChoosePhoto.CHOOSE_PHOTO_INTENT) {
            if (data.getData() != null) {
                uploadImagePresenter.handleGalleryResult(data, this);
            }
        } else if (requestCode == ChoosePhoto.SELECTED_IMG_CROP) {
            uploadImagePresenter.handleCropResult();
        }

        if (requestCode == UploadImagePresenter.PICK_IMAGE) {
            Uri picUri = data.getData();
            uploadImagePresenter.setUrlPath(picUri);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void AddDetails ()
    {
        mPresenter.OnAddCarDetails(txtcarnumber.getText().toString(),txtcarmodel.getText().toString(),txtnumberofpax.getText().toString(),mUri);
    }

    @Override
    public void onNext(String msg, Boolean status) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void showError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String error) {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void dismissProgress() {

    }

    @Override
    public void updateImageView(Uri uri) {
        mUri=uri;
        mHandler.post(() -> img_add_car.setImageURI(uri));
        checkImage = true;
        //setButtonState();
    }

    @Override
    public void onContinueAction() {

    }
}
