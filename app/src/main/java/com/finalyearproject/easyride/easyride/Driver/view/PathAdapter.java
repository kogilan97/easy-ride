package com.finalyearproject.easyride.easyride.Driver.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.finalyearproject.easyride.R;
import com.finalyearproject.easyride.easyride.BookJourney.model.Path;
import com.finalyearproject.easyride.easyride.Driver.model.PathInfo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PathAdapter extends RecyclerView.Adapter<PathAdapter.PathViewHolder> {

    private LayoutInflater inflater;
    private PathItemClickListener mCallback;
    private List<PathInfo> mPathInfo;

    PathAdapter(Context context, List<PathInfo> pathInfoList){
        inflater = LayoutInflater.from(context);
        this.mPathInfo = pathInfoList;
    }

    @NonNull
    @Override
    public PathAdapter.PathViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_path_items,parent,false);
        return new PathViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PathAdapter.PathViewHolder holder, int position) {
        PathInfo pathInfo = mPathInfo.get(position);
        holder.path_name.setText(pathInfo.getPathName());
        holder.path_date.setText(pathInfo.getDateTime());
    }

    @Override
    public int getItemCount() {
        int size = 0;
        if(mPathInfo.size()>3)
            size=3;
        else
            size = mPathInfo.size();
        return size;
    }

    public class PathViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.path_name)
        TextView path_name;
        @BindView(R.id.path_date)
        TextView path_date;

        public PathViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public void setOnItemClickListener(final PathItemClickListener listener) {
        mCallback = listener;
    }

    public interface PathItemClickListener {
        void onItemClick(String journeyID);
    }
}
