package com.finalyearproject.easyride.easyride.EditProfile.presenter;

import com.finalyearproject.easyride.easyride.LoginRegister.model.UserInfo;

public class EditProfileInterface {
    interface Presenter
    {
      void onFetchDetails();
    }
   public interface View
    {
        void onNext(String msg, Boolean status, UserInfo userInfoClass);

        void showError(String msg);
    }
}
