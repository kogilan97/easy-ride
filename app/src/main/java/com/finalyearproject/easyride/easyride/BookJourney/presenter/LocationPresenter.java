package com.finalyearproject.easyride.easyride.BookJourney.presenter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.finalyearproject.easyride.EasyRideApplication;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Locale;

public class LocationPresenter implements LocationListener, ResultCallback<LocationSettingsResult> {

    LocationManager locationManager;
    View mView;
    WeakReference<Activity> mActivity;
    WeakReference<Fragment> mFragment;
    WeakReference<FragmentActivity> mFragmentActivity;

    public final static int MY_PERMISSIONS_ACCESS_FINE_LOCATION = 101;
    private static final long MIN_TIME = 40000;
    private static final float MIN_DISTANCE = 1000;

    public LocationPresenter(View view, Activity activity) {
        this.mView = view;
        this.mActivity = new WeakReference<>(activity);

        checkPermissions(mActivity.get());
    }

    public LocationPresenter(View view, Fragment fragment) {
        this.mView = view;
        this.mFragment = new WeakReference<>(fragment);
        this.mActivity = new WeakReference<>(fragment.getActivity());

        checkPermissions(mFragment.get().getActivity());
    }

    @SuppressLint("MissingPermission")
    public Location getLastKnownLocation() {
        LocationManager mLocationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        Location bestLocation = null;
        Location l = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (l == null) {
            l = mLocationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        }

        if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
            bestLocation = l;
        }
        return bestLocation;
    }

    public void checkForGPS() {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(this);
    }

    public void stopLocationUpdates() {
        locationManager.removeUpdates(this);
    }

    public void initLocationUpdates() {

        if (mActivity.get() == null) {
            return;
        }

        locationManager = (LocationManager) mActivity.get().getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(mActivity.get(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(mActivity.get(), Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this);
        }
    }

    public void checkPermissions(Activity activity) {
        if (ActivityCompat.checkSelfPermission(mActivity.get(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(mActivity.get(), Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (mFragment != null && mFragment.get() != null) {
                mFragment.get().requestPermissions(
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_ACCESS_FINE_LOCATION);
            } else {
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_ACCESS_FINE_LOCATION);
            }
        } else {
            initLocationUpdates();
            mView.onPermissionSuccess();
        }
    }

    public Address getLocationAddress(Location location) throws IOException {

        Address result = null;
        List<String> providerList = locationManager.getAllProviders();
        if (null != location && null != providerList && providerList.size() > 0) {
            double longitude = location.getLongitude();
            double latitude = location.getLatitude();
            Geocoder geocoder = new Geocoder(EasyRideApplication.getContext(), Locale.getDefault());

            List<Address> listAddresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (null != listAddresses && listAddresses.size() > 0) {
                result = listAddresses.get(0);
            }
        }

        return result;
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_ACCESS_FINE_LOCATION) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initLocationUpdates();
                mView.onPermissionSuccess();
                onLocationChanged(getLastKnownLocation()); // todo
            } else {
                mView.onPermissionFailed();
            }
        }
    }

    protected Context getContext() {
        return EasyRideApplication.getContext();
    }

    public void onDetach() {
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    public void onDestroyView() {
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mView.onLocationChanged(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        initLocationUpdates();
    }

    @Override
    public void onProviderDisabled(String provider) {
        checkForGPS();
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult result) {
        final Status status = result.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                initLocationUpdates();
                mView.locationSuccess(result);
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                mView.resolutionRequired(result);
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                mView.locationUnavailable();
                break;
        }
    }

    public interface View {
        void onLocationChanged(Location location);

        void onPermissionSuccess();

        void onPermissionFailed();

        void locationUnavailable();

        void locationSuccess(LocationSettingsResult result);

        void resolutionRequired(LocationSettingsResult result);

    }
}
