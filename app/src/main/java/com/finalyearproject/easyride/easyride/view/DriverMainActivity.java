package com.finalyearproject.easyride.easyride.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.finalyearproject.easyride.EasyRideConstants;
import com.finalyearproject.easyride.R;
import com.finalyearproject.easyride.SwitchRoleActivity;
import com.finalyearproject.easyride.easyride.CarManagement.view.DriverCarsFragment;
import com.finalyearproject.easyride.easyride.Driver.view.DriverHomeFragment;
import com.finalyearproject.easyride.easyride.Earnings.view.DriverEarningFragment;
import com.finalyearproject.easyride.easyride.EditProfile.view.RiderProfileFragment;
import com.finalyearproject.easyride.easyride.Journey.view.JourneyFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class DriverMainActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener, SwitchRoleActivity.OnSwitchRoleListener{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_main);
        loadFragment(new DriverHomeFragment());
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        SwitchRoleActivity.setOnFinishActivityListener(this);
    }

    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem){
        Fragment fragment = null;

        switch (menuItem.getItemId()) {
            case R.id.navigation_home:
                fragment = new DriverHomeFragment();
                break;

            case R.id.navigation_journey:
                fragment = new JourneyFragment();
                break;

            case R.id.navigation_profile:
                fragment = new RiderProfileFragment();
                break;

            case R.id.navigation_car:
                fragment = new DriverCarsFragment();
                break;

            case R.id.navigation_earnings:
                fragment = new DriverEarningFragment();
                break;


        }

        return loadFragment(fragment);
    }

    @Override
    public void onSwitchRole(int role) {
        if (role == EasyRideConstants.ROLE_USER) {
            startActivity(new Intent(this, RiderMainActivity.class));
            finish();
        }
    }
}
