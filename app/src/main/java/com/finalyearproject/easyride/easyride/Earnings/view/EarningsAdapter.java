package com.finalyearproject.easyride.easyride.Earnings.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.finalyearproject.easyride.R;
import com.finalyearproject.easyride.easyride.CarManagement.model.CarDetailsClass;
import com.finalyearproject.easyride.easyride.Driver.model.PathInfo;
import com.finalyearproject.easyride.easyride.Driver.view.PathAdapter;
import com.finalyearproject.easyride.easyride.Earnings.model.EarningInfo;
import com.finalyearproject.easyride.easyride.LoginRegister.model.UserInfo;

import java.util.List;

public class EarningsAdapter extends RecyclerView.Adapter<EarningsAdapter.EarningsViewHolder> {



    private List<EarningInfo> mEarningInfo;


    public EarningsAdapter(Context context, List<EarningInfo> earnings) {

        this.mEarningInfo = earnings;
    }

    @NonNull
    @Override
    public EarningsAdapter.EarningsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.earning_items,parent,false);
        return new EarningsAdapter.EarningsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EarningsAdapter.EarningsViewHolder holder, int position) {

        EarningInfo earningInfo = mEarningInfo.get(position);
        holder.txtEarningAmount.setText("RM " + earningInfo.getAmount());
        holder.txtEarningDate.setText("Date & Time : " + earningInfo.getDateTime());
        holder.txtEarningStatus.setText( earningInfo.getDriverResponse());
    }

    @Override
    public int getItemCount() {
        return mEarningInfo.size();
    }

    public class EarningsViewHolder extends RecyclerView.ViewHolder {

        private TextView txtEarningAmount;
        private TextView txtEarningStatus;
        private TextView txtEarningDate;

        public EarningsViewHolder(@NonNull View itemView) {
            super(itemView);
            txtEarningAmount = itemView.findViewById(R.id.earning_amount);
            txtEarningStatus = itemView.findViewById(R.id.earning_status);
            txtEarningDate = itemView.findViewById(R.id.earning_date);
        }
    }
}
