package com.finalyearproject.easyride.easyride.Payment.di.module;

import com.finalyearproject.easyride.di.scope.ActivityScope;
import com.finalyearproject.easyride.easyride.Payment.presenter.PaymentContract;

import dagger.Module;
import dagger.Provides;

@Module
public class PaymentPresenterModule {
    private PaymentContract.View mView;

    public PaymentPresenterModule(PaymentContract.View view){
        this.mView = view;
    }

    @Provides
    @ActivityScope
    PaymentContract.View provideView(){
        return mView;
    }
}
