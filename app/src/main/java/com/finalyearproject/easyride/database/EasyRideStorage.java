package com.finalyearproject.easyride.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class EasyRideStorage {

    private SharedPreferences sharedPreferences;
    private static final EasyRideStorage sInstance = new EasyRideStorage();

    public static synchronized EasyRideStorage getInstance() {
        return EasyRideStorage.sInstance;
    }

    private static final String PREF_EMAIL_ADDR = "email";
    private static final String PREF_LOGGED_IN = "logged_in";
    private static final String PREF_AUTH_TOKEN = "auth_token";
    private static final String PREF_USERNAME = "username";
    private static final String PREF_APP_ROLE = "role";
    private static final String PREF_USER_ID = "userID";
    private static final String PREF_CREDIT_CARD_ADDED="credit_card";
    private static final String PREF_CURRENT_ROLE = "current_role";

    public EasyRideStorage() {
        super();
    }

    public void init(final Context context) {
        this.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public String getPrefUserId(){return  getString(PREF_USER_ID);}

    public void setPrefUserId(final String userId){setString(PREF_USER_ID,userId);}

    public boolean getPrefLoggedIn() {
        return getBoolean(PREF_LOGGED_IN);
    }

    public void setPrefLoggedIn(final boolean loggedIn) {
        setBoolean(PREF_LOGGED_IN, loggedIn);
    }

    public String getPrefUsername() {
        return getString(PREF_USERNAME);
    }

    public void setPrefUsername(final String username) {
        setString(PREF_USERNAME, username);
    }

    public void setAuthToken(final String token) {
        setString(PREF_AUTH_TOKEN, token);
    }

    public String getAuthToken() {
        return getString(PREF_AUTH_TOKEN);
    }

    public String getPrefAppRole() {
        return getString(PREF_APP_ROLE);
    }

    public void setPrefAppRole(final String appRole) {
        setString(PREF_APP_ROLE, appRole);
    }

    public String getPrefEmailAddr() {
        return getString(PREF_EMAIL_ADDR);
    }

    public void setPrefEmailAddr(final String emailAddr) {
        setString(PREF_EMAIL_ADDR, emailAddr);
    }

    public boolean getIsCreditCardAdded() {
        return getBoolean(PREF_CREDIT_CARD_ADDED);
    }

    public void setCreditCardAdded(final boolean flag) {
        setBoolean(PREF_CREDIT_CARD_ADDED, flag);
    }

    public void setCurrentRole(final int role) {
        setInt(PREF_CURRENT_ROLE, role);
    }

    public int getCurrentRole() {
        return getInt(PREF_CURRENT_ROLE);
    }

    /****************
     * ***** CONVENIENCE ******
     * **************
     */

    private void setBoolean(final String key, final boolean value) {
        if (sharedPreferences != null) {
            sharedPreferences.edit().putBoolean(key, value).apply();
        }
    }

    private boolean getBoolean(final String key) {
        return sharedPreferences != null && sharedPreferences.getBoolean(key, false);
    }

    private void setInt(final String key, final int value) {
        if (sharedPreferences != null) {
            sharedPreferences.edit().putInt(key, value).apply();
        }
    }

    private int getInt(final String key) {
        if (sharedPreferences != null) {
            return sharedPreferences.getInt(key, 0);
        } else {
            return 0;
        }
    }

    private void setLong(final String key, final long value) {
        if (sharedPreferences != null) {
            sharedPreferences.edit().putLong(key, value).apply();
        }
    }

    private long getLong(final String key) {
        if (sharedPreferences != null) {
            return sharedPreferences.getLong(key, 0);
        } else {
            return 0;
        }
    }

    private void setString(final String key, final String value) {
        if (sharedPreferences != null) {
            sharedPreferences.edit().putString(key, value).apply();
        }
    }

    private String getString(final String key) {
        if (sharedPreferences != null) {
            return sharedPreferences.getString(key, null);
        } else {
            return null;
        }
    }

    private void setFloat(final String key, final float value) {
        if (sharedPreferences != null) {
            sharedPreferences.edit().putFloat(key, value).apply();
        }
    }

    private float getFloat(final String key) {
        if (sharedPreferences != null) {
            return sharedPreferences.getFloat(key, 0);
        } else {
            return 0;
        }
    }

}
