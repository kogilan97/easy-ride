package com.finalyearproject.easyride.di.qualifier;

import javax.inject.Qualifier;

@Qualifier
public @interface ApplicationContext {
}
