package com.finalyearproject.easyride.di.module;


import com.finalyearproject.easyride.data.UserModalClass;
import com.finalyearproject.easyride.easyride.CarManagement.model.MainCarsClass;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;


public interface APIInterface {

    @POST("/users/login")
    Observable<UserModalClass> getUserDetails(@Body RequestBody body);

    @POST("/users/login")
    Observable<UserModalClass> getUserLoginDetails(@Body RequestBody body);

    @POST("/users/register")
    Observable<UserModalClass> getUserRegistration(@Body RequestBody body);

    @GET("/users")
    Observable<UserModalClass> getUser();

    @GET("/journeys/viewjourneys/{id}")
    Observable<UserModalClass> getUserJourney(@Header("x-access-token") String authToken, @Header("userid") String userID, @Path("id") String journeyStatus);

    @GET("/journeys/driver/{id}")
    Observable<UserModalClass> getDriverJourney(@Header("x-access-token") String authToken, @Header("userid") String userID, @Path("id") String journeyStatus);

    @POST("/users/getprofiledetails")
    Observable<UserModalClass> getProfileDetails(@Body RequestBody body);

    @POST("/users/emailVerification")
    Observable<UserModalClass> verifyEmail(@Body RequestBody body);

    @GET("/cars/viewallcars/{userID}")
    Observable<MainCarsClass> viewAllCars(@Path("userID") String userID);

    @POST("/cars/addnewcar")
    Observable<MainCarsClass> addNewCar(@Body RequestBody body);

    @PUT("/cars/addcarimage/{id}")
    @Multipart
    Observable<UserModalClass> uploadCarImage(@Header("x-access-token") String token,
                                          @Path("id") String id,
                                          @Part MultipartBody.Part file);

    @DELETE("/cars/deletecar/{carId}")
    Observable<MainCarsClass> deleteCar(@Path("carId") String id);

    @POST("/paths/search")
    Observable<UserModalClass> getPath(@Body RequestBody object);

    @POST("/journeys/newjourney")
    Observable<UserModalClass> createJourney(@Body RequestBody object);

    @POST("/cards/calculate")
    Observable<UserModalClass> getPricing(@Header("x-access-token") String authToken,@Body RequestBody object);

    @GET("/paths/viewSearchOne/{id}")
    Observable<UserModalClass> getPathInfo(@Path("id") String pathID);

    @GET("/journeys/viewsinglejourney/{id}")
    Observable<UserModalClass> getJourneyDetails(@Header("x-access-token") String authToken, @Path("id") String journeyID);

    @GET("/journeys/driverOne/{id}")
    Observable<UserModalClass> getDriverJourneyDetails(@Header("x-access-token") String authToken, @Path("id") String journeyID);

    @POST("/users/verifiedLicense/{id}")
    Observable<UserModalClass> verifyLicense(@Header("x-access-token") String authToken,@Path("id") String userID);

    @POST("/users/makePhoneRequest/{phonenumber}")
    Observable<UserModalClass> makePhoneRequest(@Header("x-access-token") String authToken,@Path("phonenumber") String userID);

    @POST("/cards/create")
    Observable<UserModalClass> createCreditCard(@Header("x-access-token")String authToken, @Body RequestBody body);

    @POST("/cards/pay")
    Observable<UserModalClass> makePayment(@Header("x-access-token")String authToken, @Body RequestBody body);

    @POST("/users/verifiedIc/{id}")
    Observable<UserModalClass> verifyIC(@Header("x-access-token") String authToken,@Path("id") String userID);

    @DELETE("/cards/delete/{userid}")
    Observable<UserModalClass> deleteCreditCard(@Header("x-access-token")String authToken, @Path("userid")String UserId);

    @GET("/cards/getAll/{userid}")
    Observable<UserModalClass> getCreditCard(@Header("x-access-token")String authToken, @Path("userid")String prefUserId);

    @POST("/users/sendfeedback")
    Observable<UserModalClass> getFeedback(@Header("x-access-token")String authToken, @Body RequestBody body);

    @POST("/users/validatePhoneRequest")
    Observable<UserModalClass> validatePhoneRequest(@Header("x-access-token")String authToken, @Body RequestBody body);

    @GET("/paths/view/{userid}")
    Observable<UserModalClass> getAllPath(@Header("x-access-token")String authToken, @Path("userid")String prefUserId);

    @POST("/paths/create")
    Observable<UserModalClass> createPath(@Body RequestBody requestBody);

    @POST("/journeys/driverAccept/{journeyID}")
    Observable<UserModalClass> acceptJourney(@Header("x-access-token")String authToken, @Path("journeyID")String journeyID);

    @POST("/journeys/driverStart/{journeyID}")
    Observable<UserModalClass> startJourney(@Header("x-access-token")String authToken, @Path("journeyID")String journeyID);

    @POST("/journeys/driverReject/{journeyID}")
    Observable<UserModalClass> rejectJourney(@Header("x-access-token")String authToken, @Path("journeyID")String journeyID);

    @POST("/journeys/driverComplete/{journeyID}")
    Observable<UserModalClass> completeJourney(@Header("x-access-token")String authToken, @Path("journeyID")String journeyID);

    @POST("/journeys/passengerCancel/{journeyID}")
    Observable<UserModalClass> cancelJourney(@Header("x-access-token")String authToken, @Path("journeyID")String journeyID);

    @GET("/cards/earnings/{userId}")
    Observable<UserModalClass> viewEarnings(@Header("x-access-token")String authToken,@Path("userId")String userId);

}
