package com.finalyearproject.easyride.di.module;

import com.finalyearproject.easyride.BuildConfig;
import com.finalyearproject.easyride.EasyRideConstants;
import com.finalyearproject.easyride.di.scope.ApplicationScope;


import java.io.IOException;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class RetrofitModule {

    @Provides
    @ApplicationScope
 public static APIInterface getInterface(Retrofit retrofit) {
        return retrofit.create(APIInterface.class);
    }

    @Provides
    @ApplicationScope

    public static Retrofit getRetrofit(OkHttpClient okHttpClient) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(EasyRideConstants.EBHost)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient)
                .build();

        return retrofit;
    }

    Retrofit getRetrofitWithBaseUrl(OkHttpClient okHttpClient, String baseUrl) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @Provides
    @ApplicationScope
    OkHttpClient getOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor) {
        Interceptor interceptor = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .header("Accept", "application/json")
                        .header("Content-Type", "application/json")
                        .build();
                return chain.proceed(newRequest);
            }
        };

        return new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor(httpLoggingInterceptor)
                .build();
    }

    @Provides
    @ApplicationScope
    HttpLoggingInterceptor httpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        return httpLoggingInterceptor;
    }

    private static APIInterface apiInterface;

    public static APIInterface getApiInterface() {
        if (apiInterface == null) {
            RetrofitModule retrofitModule = new RetrofitModule();
            OkHttpClient okHttpClient = retrofitModule.getOkHttpClient(retrofitModule.httpLoggingInterceptor());
            Retrofit retrofit = retrofitModule.getRetrofit(okHttpClient);
            apiInterface = retrofitModule.getInterface(retrofit);

        }
        return apiInterface;
    }

    public static APIInterface getApiInterface(String baseUrl) {

        RetrofitModule retrofitModule = new RetrofitModule();
        OkHttpClient okHttpClient = retrofitModule.getOkHttpClient(retrofitModule.httpLoggingInterceptor());
        Retrofit retrofit = retrofitModule.getRetrofitWithBaseUrl(okHttpClient, baseUrl);
        return retrofitModule.getInterface(retrofit);
    }
}