package com.finalyearproject.easyride.di.component;

import android.content.Context;

import com.finalyearproject.easyride.EasyRideApplication;
import com.finalyearproject.easyride.di.module.APIInterface;
import com.finalyearproject.easyride.di.module.ContextModule;
import com.finalyearproject.easyride.di.module.RetrofitModule;
import com.finalyearproject.easyride.di.qualifier.ApplicationContext;
import com.finalyearproject.easyride.di.scope.ApplicationScope;

import dagger.Component;

@ApplicationScope
@Component(modules = {ContextModule.class, RetrofitModule.class})
public interface ApplicationComponent {
    APIInterface getApiInterface();

    @ApplicationContext
    Context getContext();

    void injectApplication(EasyRideApplication myApplication);
}
