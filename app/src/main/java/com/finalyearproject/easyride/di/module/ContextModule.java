package com.finalyearproject.easyride.di.module;

import android.content.Context;

import com.finalyearproject.easyride.di.qualifier.ApplicationContext;
import com.finalyearproject.easyride.di.scope.ApplicationScope;

import dagger.Module;
import dagger.Provides;

@Module
public class ContextModule {
    private Context context;

    public ContextModule(Context context) {
        this.context = context;
    }

    @Provides
    @ApplicationScope
    @ApplicationContext
    public Context provideContext() {
        return context;
    }
}

