package com.finalyearproject.easyride;

import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.finalyearproject.easyride.database.EasyRideStorage;
import com.finalyearproject.easyride.easyride.view.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SwitchRoleActivity extends BaseActivity {
    private static OnSwitchRoleListener mCallback;
    private Handler mHandler = new Handler();

    @BindView(R.id.switch_img)
    ImageView mSwitchImg;
    @BindView(R.id.switch_to_tv)
    TextView mSwitchToTv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_switch_role);
        ButterKnife.bind(this);
        Runnable mRunnable = this::setAppropriateFragments;

        if (EasyRideStorage.getInstance().getCurrentRole() == EasyRideConstants.ROLE_USER) {
            mSwitchToTv.setText("Switching to driver...");
            EasyRideStorage.getInstance().setCurrentRole(EasyRideConstants.ROLE_DRIVER);
            mHandler.postDelayed(mRunnable, 1000);
        } else if (EasyRideStorage.getInstance().getCurrentRole() == EasyRideConstants.ROLE_DRIVER) {
            mSwitchToTv.setText("Switching to passenger...");
            EasyRideStorage.getInstance().setCurrentRole(EasyRideConstants.ROLE_USER);
            mHandler.postDelayed(mRunnable, 1000);
        }
    }


    private void setAppropriateFragments() {
        if (mCallback != null) {
            mCallback.onSwitchRole(EasyRideStorage.getInstance().getCurrentRole());
            finish();
        }
    }

    public static void setOnFinishActivityListener(final OnSwitchRoleListener listener) {
        mCallback = listener;
    }

    public interface OnSwitchRoleListener {
        void onSwitchRole(int role);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacksAndMessages(null);
    }
}


